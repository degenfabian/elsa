ARG VERSION

FROM silkeh/clang:${VERSION} as base
MAINTAINER Tobias Lasser <lasser@in.tum.de>
MAINTAINER David Frank <frankd@in.tum.de>

ARG VERSION=${VERSION}
ARG LLVM_PKG_VERSION

LABEL Description="Base Image for clang"
LABEL Description="Base Image for clang"
RUN (apt update -qq && apt upgrade -qqy && apt install --no-install-recommends -qqy rsync curl build-essential wget git ca-certificates libssl-dev pkg-config libomp-${VERSION}-dev \
       ninja-build python3 python3-pip python3-setuptools python3-dev python3-wheel python3-matplotlib python3-numpy libc++-${VERSION}-dev \
       llvm-${LLVM_PKG_VERSION} llvm-${LLVM_PKG_VERSION}-dev llvm-${LLVM_PKG_VERSION}-runtime \
       libclang-${LLVM_PKG_VERSION}-dev libclang-cpp${LLVM_PKG_VERSION} libclang-cpp${LLVM_PKG_VERSION}-dev)
RUN if [ $VERSION -eq 12 ]; then apt install -qqy libunwind-dev; fi
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 10
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
RUN pip3 install cmakelang

RUN (curl -LO https://github.com/Kitware/CMake/releases/download/v3.23.2/cmake-3.23.2-linux-x86_64.tar.gz \
    && tar xf cmake-3.23.2-linux-x86_64.tar.gz \
    && rm cmake-3.23.2-linux-x86_64.tar.gz \
    && apt-get remove -yq cmake \
    && rsync -avh -I --progress cmake-3.23.2-linux-x86_64/ /usr/local/ \
    && apt-get remove -yq rsync \
    && cmake --version)

ENV CC clang
ENV CXX clang++
