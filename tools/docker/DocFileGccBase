ARG VERSION

FROM gcc:${VERSION} as base
MAINTAINER Tobias Lasser <lasser@in.tum.de>
MAINTAINER David Frank <frankd@in.tum.de>

LABEL Description="Base Image for GNU GCC"

ARG VERSION=${VERSION}
ARG LLVM_PKG_VERSION

RUN ( apt-get update -qq && apt-get upgrade -qqy \
    && apt-get --no-install-recommends install -qq rsync curl build-essential wget git ca-certificates libssl-dev pkg-config \
       ninja-build python3 python3-pip python3-setuptools python3-dev python3-wheel python3-matplotlib python3-numpy \
       llvm-${LLVM_PKG_VERSION} llvm-${LLVM_PKG_VERSION}-dev llvm-${LLVM_PKG_VERSION}-runtime \
       libclang-${LLVM_PKG_VERSION}-dev libclang-cpp${LLVM_PKG_VERSION} libclang-cpp${LLVM_PKG_VERSION}-dev libunwind8 libunwind-dev \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
    && update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10 && pip3 install wheel \
    && pip3 install matplotlib numpy \
    && rm -rf /var/lib/apt/lists/* )

RUN (curl -LO https://github.com/Kitware/CMake/releases/download/v3.23.2/cmake-3.23.2-linux-x86_64.tar.gz \
    && tar xf cmake-3.23.2-linux-x86_64.tar.gz \
    && rm cmake-3.23.2-linux-x86_64.tar.gz \
    && rsync -avh -I --progress cmake-3.23.2-linux-x86_64/ /usr/local/ \
    && apt-get remove -yq rsync \
    && cmake --version)

ENV CC gcc
ENV CXX g++
