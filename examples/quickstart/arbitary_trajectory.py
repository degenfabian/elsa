import numpy as np
import pyelsa as elsa
from scipy.spatial.transform import Rotation as R

vol_shape = np.array([64] * 3)
vol_spacing = np.array([1.] * 3)
det_shape = np.array([100] * 3)
det_spacing = np.array([1.] * 2)

dist_source_origin = 1000.
dist_origin_detector = 100.

rot_dir = np.array([0, 1, -0.3])
rot_dir = rot_dir / np.linalg.norm(rot_dir)

# rotation around axis
angles = np.linspace(0, 360, 360, endpoint=False)
rot_matrices = [R.from_rotvec(
    angle * rot_dir, degrees=True).as_matrix() for angle in angles]

geom = []
for rot_mat in rot_matrices:
    g = elsa.Geometry(
        dist_source_origin, dist_origin_detector,
        vol_shape, det_shape,
        rot_mat,
        vol_spacing,  # optional, (default [1, 1, 1])
        det_spacing,  # optional, (default [1, 1])
        [0, 0],       # optional, detector shift (default [0, 0])
        [0, 0, 0]     # optional, center of rotation offset (default [0, 0, 0])
    )
    geom.append(g)

sino_desc = elsa.PlanarDetectorDescriptor(det_shape, geom)
