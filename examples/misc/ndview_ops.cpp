#include "elsa.h"
#include <iostream>
#include <iomanip>

using namespace elsa;

template <typename T>
void pretty_print(const NdView<T>& view)
{
    auto& shape = view.shape();
    /* assuming 2d data */
    if (shape.size() != 2) {
        std::cout << "Failed to pretty print tensor, unexpected dimensionality" << std::endl;
    }
    /* rows in elsa are the second index */
    for (size_t row = 0; row < shape(1); row++) {
        for (size_t col = 0; col < shape(0); col++) {
            std::cout << std::setfill(' ') << std::setw(2) << view(col, row) << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{
    thrust::universal_vector<int> data(27);
    for (size_t i = 0; i < 9; ++i)
        data[i] = i;

    int* raw_data = thrust::raw_pointer_cast(data.data());
    IndexVector_t strides(2), shape(2);
    strides << 1, 3;
    shape << 3, 3;

    NdView<int> view(raw_data, shape, strides, [&]() { /* do nothing */ });

    std::cout << "Initial data:" << std::endl;
    pretty_print(view);

    /* subtract 4 from all elements of the view */
    view = view - 4;
    std::cout << "Center around 0 by subtracting constant 4:" << std::endl;
    pretty_print(view);

    /* binary operation on two NdViews, equivalent to view * 2 */
    view = view + view;
    std::cout << "Double values in view by computing view + view:" << std::endl;
    pretty_print(view);

    /* halve values again */
    view = view / 2;
    std::cout << "Halve values in view by computing view / 2:" << std::endl;
    pretty_print(view);

    /* filtered assignment */
    view[view >= 3 || view <= -3] = 0;
    std::cout << "Zero the values outside of range [-3, 3] "
                 "with view[view >= 3 || view <= -3] = 0:"
              << std::endl;
    pretty_print(view);
    view = 2 + view;
}