"""This is a reconstruction of the walnut provided by the Finnish Inverse
Problems Society.

The DOI of the data is 10.5281/zenodo.6986012. It can be found at
https://zenodo.org/record/6986012. The run the script you need to download
the zip files and extract them in a common folder.

The walnut data is a well scanned reconstruction with a lot of data. Little
preprocessing is needed, with the expection of some clean up and the
log-likelyhood transform.

In this example, the constrained optimization problem with the least squares
data term with a non-negativity constraint is solved using APGD/FISTA.

With small modifications, the scripts is usable for the seashell and and pine
cone dataset also provided by the Finnish Inverse Problems Society. They
can be found with the DOIs 10.5281/zenodo.6983008 and 10.5281/zenodo.6985407
respectively.
"""

import argparse
from pathlib import Path
import math

from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import numpy as np
import pyelsa as elsa
import tifffile
import tqdm

from fips_data import *

# Taken from the txt file provided with the data
sourceToDetector = 553.74
sourceToOrigin = 210.66
originToDetector = sourceToDetector - sourceToOrigin

detectorSpacing = np.asarray([0.050, 0.050])


def reconstruct(A, b, niters=70):
    """Just a basic reconstruction which looks nice"""
    h = elsa.IndicatorNonNegativity(A.getDomainDescriptor())

    # Lipschitz constant of the gradient of least squares it the largest eigenvalue of (A^T A)
    L = elsa.powerIterations(elsa.adjoint(A) * A) * 1.1

    solver = elsa.POGM(A, b, h, mu=1 / L)
    return solver.solve(niters)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="elsa Reconstruction of public datasets provided by FIPS (e.g. Walnut and Seashell)"
    )
    parser.add_argument(
        "path",
        type=Path,
        help="Path to look for tif files",
    )
    parser.add_argument(
        "-i",
        "--iters",
        type=int,
        default=50,
        help="Number of iterations for the reconstruction algorithm",
    )
    parser.add_argument(
        "-b",
        "--binning",
        type=int,
        default=4,
        help="Binning factor, must be factor of two (default 4)",
    )
    parser.add_argument(
        "-s",
        "--step",
        type=int,
        default=1,
        help="Take every n-th image only (default 1)",
    )
    parser.add_argument(
        "--dataset", type=str, default="walnut", help=""
    )
    parser.add_argument(
        "--padding", type=int, default=0, help="Add additional padding (default 0)"
    )
    parser.add_argument(
        "--resolution",
        type=float,
        default=1,
        help="Change the resolution of the reconstruction (default 1.0)",
    )

    args = parser.parse_args()

    if not elsa.cudaProjectorsEnabled():
        import warnings

        warnings.warn(
            "elsa is build without CUDA support. This reconstruction may take a long time."
        )

    binning = args.binning
    padding = args.padding
    resolution = args.resolution

    if args.dataset == "chicken":
        angle_factor = 1.
    else:
        angle_factor = 2.

    # load the dataset
    projections, angles = load_dataset(
        path=args.path, binning=binning, step=args.step, padding=padding, dataset=args.dataset, angle_factor=angle_factor
    )

    # Compute magnification
    magnification = sourceToDetector / sourceToOrigin

    # Determine good size and spacing for volume
    vol_spacing = (detectorSpacing[0] * binning) / (magnification * resolution)
    size = int(
        math.ceil((np.max(projections.shape[:-1]) - padding) * resolution))

    volume_descriptor = elsa.VolumeDescriptor([size] * 3, [vol_spacing] * 3)

    # define the sinogram for elsa
    sinogram = define_sino_descriptor(
        projections, angles, volume_descriptor, binning * detectorSpacing, sourceToOrigin, originToDetector)
    sino_descriptor = sinogram.getDataDescriptor()

    # Set the forward and backward projector
    if elsa.cudaProjectorsEnabled():
        projector = elsa.JosephsMethodCUDA(volume_descriptor, sino_descriptor)
    else:
        projector = elsa.JosephsMethod(volume_descriptor, sino_descriptor)

    # Perform reconstruction
    recon = reconstruct(projector, sinogram, args.iters)

    # Do a forward projection of the reconstruction. If this is close to the
    # original projections, the reconstruction should be good as well
    forward = projector.apply(recon)

    # Now just show everything...
    nprecon = np.array(recon)
    npforward = np.array(forward)

    # save reconstruction
    tifffile.imwrite(
        f"recon_fips_walnut_{args.iters}.tif",
        nprecon,
    )

    # And now visualize everything

    def add_colorbar(fig, ax, im):
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, cax=cax, orientation="vertical")

    fig = plt.figure(constrained_layout=True)

    fig1, fig2 = fig.subfigures(nrows=2, ncols=1)

    axs = fig1.subplots(nrows=1, ncols=2)
    for col, (ax, slice, title) in enumerate(
        zip(
            axs,
            [nprecon[:, size // 2, :], nprecon[:, :, size // 2]],
            ["Axial center slice", "Coronal center slice"],
        )
    ):
        im = ax.imshow(slice, cmap="gray")
        ax.set_title(title)
        # Hide ticks
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        add_colorbar(fig1, ax, im)

    fig2.suptitle(
        "Compare the forward projected reconstruction with the original projections\nFor a good reconstruction, the two should be close"
    )
    axs = fig2.subplots(nrows=1, ncols=2)
    projection = projections.shape[0] // 2
    for col, (ax, slice, title) in enumerate(
        zip(
            axs,
            [npforward[projection, :, :], projections[projection, :, :]],
            ["Forward projected reconstruction", "Original projection"],
        )
    ):
        im = ax.imshow(slice, cmap="gray")
        ax.set_title(title)
        # Hide ticks
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        add_colorbar(fig2, ax, im)

    plt.show()
