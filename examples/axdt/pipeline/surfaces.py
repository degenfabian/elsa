#!/usr/bin/env python3
import argparse
from pathlib import Path
from typing import Tuple

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from utils import *

import pyelsa as elsa

mplstyle.use('fast')


def visualizeOrientationDensity(ax: Axes3D, eta: np.ndarray, zScore: float = .5, res: Tuple[int, int] = (20, 20), truncL: int = 4, color='b') -> None:

    phi_res, theta_res = res

    _, sz, sy, sx = eta.shape

    # Directions to sample at with associated weights and edge relation
    directions, _, (x, y, z) = generate_sphere(*res)

    etaDC = makeEtaContainer(eta, truncL)

    funkRadon = elsa.axdt.FunkRadonTransform(etaDC.getDataDescriptor())
    evaluate = elsa.axdt.SphericalFunctionTransform(
        etaDC.getDataDescriptor(), directions)

    transformedCoeffs = funkRadon.apply(etaDC)
    orientationDensity = evaluate.apply(transformedCoeffs)

    r = np.asarray(orientationDensity)

    r = abs(r)
    r /= np.max(r)
    r /= 2  # Divide by two as distance between to surfaces is one

    # Decide whether to plot a surface based on its maximal value and the relation to the statistics of the maximal values
    rMax = np.max(r, axis=0)
    sigma_r = np.std(rMax)
    threshold = np.mean(rMax) + zScore * sigma_r

    # Matplotlib needs a closed surface, so we need to duplicate the poles and half a meridian
    zeros = np.zeros((1, phi_res))
    ones = np.ones((1, phi_res))

    x = np.concatenate((zeros, x, zeros), axis=0)
    y = np.concatenate((zeros, y, zeros), axis=0)
    z = np.concatenate((ones, z, -ones), axis=0)

    x = np.append(x, x[:, 0][:, np.newaxis], axis=1)
    y = np.append(y, y[:, 0][:, np.newaxis], axis=1)
    z = np.append(z, z[:, 0][:, np.newaxis], axis=1)

    for ix, iy, iz in itertools.product(range(0, sx), range(0, sy), range(0, sz)):

        mag = r[:, iz, iy, ix].reshape(
            (phi_res, theta_res, 1)).squeeze()

        polemean = np.mean(mag, axis=1)[0]

        mag = np.concatenate((ones*polemean, mag, ones*polemean), axis=0)
        mag = np.append(mag, mag[:, 0][:, np.newaxis], axis=1)

        if mag.max() > threshold:

            ax.plot_surface(ix+x*mag, iy+y*mag, iz+z*mag,
                            color=color, alpha=0.5, zorder=10)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Surface Visualization')

    parser.add_argument("--eta", "-c", type=Path,
                        help="Dark field signal coefficients to visualize")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--interactive", "-i", action="store_true",
                        help="Show interactive plot")

    parser.add_argument("--every", "-e", type=int, default=1,
                        help="Plot surfaces for every nth voxel")

    parser.add_argument("--phi", type=int, default=20,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=20,
                        help="Resolution in theta")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    parser.add_argument("--max-l", "-l", type=int, default=4,
                        help="Truncate degrees above this l")

    args = parser.parse_args()

    if args.max_l is not None and args.max_l % 2 != 0:
        print("Truncation l must be even!")
        sys.exit(1)

    if args.output is None and not args.interactive:
        print("Need to supply output directory or --interactive!")
        sys.exit(1)

    efiles = [args.eta]  # , "groundtruth.npy"]

    colors = ['b', 'r']
    i = 0

    fig = plt.figure(figsize=(10, 10), constrained_layout=True)

    ax = fig.add_subplot(projection="3d")
    # ax.view_init(elev=90, azim=0, roll=90)

    for eta_file in efiles:

        print(f"Processing {eta_file}...")

        eta = np.load(eta_file)
        eta = eta[:, ::args.every, ::args.every, ::args.every]

        visualizeOrientationDensity(
            ax, eta, args.zscore, (args.theta, args.phi), args.max_l, colors[i])

        i += 1

        set_axes_equal(ax)
        ax.set_box_aspect([1, 1, 1])

        fig.suptitle(f"AXDT scattering surface visualization - {eta_file}")
        if args.output is not None:
            fig.savefig(args.output)

        if not args.interactive:
            plt.close(fig)

    if args.interactive:
        plt.show()
