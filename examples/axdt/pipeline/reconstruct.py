#!/usr/bin/env python3
import argparse
from pathlib import Path
from typing import Any, List, Optional, Tuple

import numpy as np
import yaml
from tqdm import trange
from utils import *

import pyelsa as elsa

silence()


def optimize(model: str, loss: elsa.Functionalf, x0: Optional[elsa.DataContainer], args: argparse.Namespace, log: Optional[List[str]]):

    # linesearch = elsa.NewtonRaphson(loss, 1)
    # linesearch = elsa.BarzilaiBorwein(fn)
    solver = elsa.FGM(loss)

    x = solver.setup(x0)

    r = trange(args.iters, desc=f"Model {model}")

    if log is not None:
        log.append(solver.formatHeader())

    for i in r:
        if args.save_every is not None and i % args.save_every == 0:
            save(args, model, i, x, "reco")
            save(args, model, i, loss.getGradient(x), "grad")

        if log is not None:
            log.append(solver.formatStep(x))

        x = solver.step(x)

    return x


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Reconstruction')

    parser.add_argument("input", type=Path, help="Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--reco-dir", "-r", required=True, type=Path,
                        help="Path to store reconstruction at")

    parser.add_argument("--binning", "-b", type=int, default=2,
                        help="Binning factor reducing resolution but also computational cost")
    parser.add_argument("--period", "-p", type=int,
                        default=8, help="Grating periods")
    parser.add_argument("--models", "-m,", required=True, nargs="+", type=str, choices=[
                        "0", "1", "2a", "2b"], help="Noise model(s): \n\t0 -> Gaussian log(d), \n\t1 -> Gaussian d, \n\t2a~ Rician b (approximated by Gaussian), \n\t2b ~ and Rician b")
    parser.add_argument("--iters", "-i", type=int, default=10,
                        help="Number of iterations for reconstruction")

    parser.add_argument("--log", "-l", type=Path,
                        help="Path to dump log to")

    parser.add_argument("--save-every", "-e", type=int,
                        help="Number of iterations between savepoints")

    parser.add_argument("--phi", type=int, default=20,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=20,
                        help="Resolution in theta")

    parser.add_argument("--eta0", type=Path, help="Initial guess for eta")
    parser.add_argument("--mu0", type=Path, help="Initial guess for mu")
    parser.add_argument(
        "--numerical", help="Use numerical weights", action="store_true")

    parser.add_argument("--solver", "-s", type=str, default="FGM", choices=solvers.keys(),
                        help="Solver to use for reconstruction")

    args = parser.parse_args()
    args.algo = "FGM"
    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    givenSetup = loadData(args)

    log = [str(args)] if args.log is not None else None

    for model in args.models:

        x0 = loadStart(args, model)
        loss = setupProblem(model, *givenSetup, args.period)

        if log is not None:
            log.append(titles[model])

        reconstruction = optimize(model, loss, x0, args, log)
        save(args, model, args.iters, reconstruction, "reco")

    if args.log is not None:
        with open(args.log, "w") as f:
            f.write("\n".join(log))
