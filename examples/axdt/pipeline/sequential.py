#!/usr/bin/env python3
import argparse
from pathlib import Path
from typing import Any, List, Optional, Tuple

import numpy as np
import yaml
from tqdm import trange
from utils import *

import pyelsa as elsa

silence()


def setupSeparation(model: str, axdt_op: elsa.axdt.AXDTOperator, absorp_op: elsa.LinearOperator, ffa: DC, ffb: DC, a: DC, b: DC, period: int):

    image = -elsa.log(a/ffa)

    g = elsa.IndicatorNonNegativity(absorp_op.getDomainDescriptor())
    solver = elsa.APGD(absorp_op, image, g)
    print("Reconstructing mu...")
    mu = solver.solve(1000)

    if(args.use_given_a):
        a = elsa.exp(-absorp_op.apply(mu)) * ffa

    approx = model == "2a"

    loss = elsa.RicianLoss(ffa, ffb, a, b, absorp_op,
                           axdt_op, period, approx)

    return loss, mu


def sequential(loss: elsa.RicianLoss, mu: DC, args: argparse.Namespace):

    # argmax Pr[d | a, b]

    # linesearch = elsa.NewtonRaphson(loss, 1)
    # linesearch = elsa.BarzilaiBorwein(fn)
    solver = elsa.FGM(loss)

    x = solver.setup()

    x.getBlock(0).set(mu)

    r = trange(args.iters, desc=f"Model {model}")

    for i in r:
        if args.save_every is not None and i % args.save_every == 0:
            save(args, model, i, x, "reco")
            save(args, model, i, loss.getGradient(x), "grad")

        x = solver.step(x)

    return x


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Reconstruction')

    parser.add_argument("input", type=Path, help="Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--reco-dir", "-r", required=True, type=Path,
                        help="Path to store reconstruction at")

    parser.add_argument("--binning", "-b", type=int, default=2,
                        help="Binning factor reducing resolution but also computational cost")
    parser.add_argument("--period", "-p", type=int,
                        default=8, help="Grating periods")
    parser.add_argument("--models", "-m,", required=True, nargs="+", type=str, choices=[
        "2a", "2b"], help="Noise model(s):\n\t2a~ Rician b (approximated by Gaussian), \n\t2b ~ and Rician b")
    parser.add_argument("--iters", "-i", type=int, default=10,
                        help="Number of iterations for reconstruction")

    parser.add_argument("--use-given-a", action="store_true", help="Don't use projection of reconstructed mu to reconstruct eta")

    parser.add_argument("--save-every", "-e", type=int,
                        help="Number of iterations between savepoints")

    parser.add_argument(
        "--numerical", help="Use numerical weights", action="store_true")

    args = parser.parse_args()
    args.algo = "FGM"
    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    givenSetup = loadData(args)

    for model in args.models:

        loss, mu = setupSeparation(model, *givenSetup, args.period)

        reconstruction = sequential(loss, mu, args)
        save(args, model, args.iters, reconstruction, "reco")

