#!/usr/bin/env python3
import argparse

from pathlib import Path
from typing import Tuple

import yaml

import numpy as np
from raster_geometry import cylinder

import pyelsa as elsa
import shutil

from utils import *


def phantom(volDesc: elsa.VolumeDescriptor) -> Tuple[elsa.DataContainer, elsa.DataContainer]:

    np.random.seed(123)

    volX, volY, volZ = volDesc.getNumberOfCoefficientsPerDimension()[::-1]
    volX = int(volX)
    volY = int(volY)
    volZ = int(volZ)

    etaShape = (15, volZ, volY, volX)
    volShape = (volZ, volY, volX)

    print(f"Generating cylindrical phantom in volume {volShape}")

    height = int(volZ / 2)
    radius = int(0.25 * (min(volX, volY) // 2))

    cyl = cylinder(volShape,
                   height,
                   radius,
                   axis=0,
                   position=0.5,
                   smoothing=True)

    eta = np.zeros(etaShape)

    # l = 2, m = 0 should give fibers in z direction?
    # Arbitrarily rescale by 1000 to avoid overflow in exp...
    eta[0] = cyl / 100
    eta[3] = -cyl / 100
    mu = cyl / 1000

    sphDesc = elsa.SphericalCoefficientsDescriptor(
        volDesc, elsa.axdt.Symmetry.Even, 4)

    return elsa.DataContainer(mu, volDesc), elsa.DataContainer(eta, sphDesc)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Inverse Criminal')

    parser.add_argument("input", type=Path, help="Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--output", "-o", required=True, type=Path,
                        help="Path to store synthetic data at")

    parser.add_argument("--period", "-p", type=int,
                        default=8, help="Grating periods")

    args = parser.parse_args()

    # Use all available data
    args.numerical = False
    args.binning = 1

    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    axdt_op, absorp_op, ffa, ffb, realA, realB = loadData(args)

    ffa = np.asarray(ffa)
    ffb = np.asarray(ffb)

    mu, eta = phantom(absorp_op.getDomainDescriptor())

    np.save(args.output / "eta.npy", np.asarray(eta)[:, ::4, ::4, ::4])

    print("Applying forward model...")

    log_d = -np.asarray(axdt_op.apply(eta))

    if np.any(np.asarray(log_d) > 0):
        print(f"Negative eta detected!: {np.max(log_d)}")

    d = np.exp(log_d)

    log_a = -np.asarray(absorp_op.apply(mu)) + np.log(ffa)

    a = np.exp(log_a)

    b = a * d * ffb / ffa

    print(f"A: min={np.min(a):.3g}", f"max={np.max(a):.3g}",
          f"{np.mean(a):.3g}±{np.std(a):.3g} ({np.mean(a)/np.std(a)})")
    print(f"B: min={np.min(b):.3g}", f"max={np.max(b):.3g}",
          f"{np.mean(b):.3g}±{np.std(b):.3g} ({np.mean(b)/np.std(b)})")
    print(f"log d: min={np.min(log_d):.3g}", f"max={np.max(log_d):.3g}",
          f"{np.mean(log_d):.3g}±{np.std(log_d):.3g} ({np.mean(log_d)/np.std(log_d)})")

    print(f"Real A: min={np.min(realA):.3g}", f"max={np.max(realA):.3g}",
          f"{np.mean(realA):.3g}±{np.std(realA):.3g} ({np.mean(realA)/np.std(realA)})")
    print(f"Real B: min={np.min(realB):.3g}", f"max={np.max(realB):.3g}",
          f"{np.mean(realB):.3g}±{np.std(realB):.3g} ({np.mean(realB)/np.std(realB)})")

    # Save data

    conf_in = args.input / args.config
    angles_in = args.input / "angles.csv"
    ffa_in = args.input / args.yaml["data"]["ffa-file"]
    ffb_in = args.input / args.yaml["data"]["ffb-file"]

    conf_out = args.output / "dataset_dci.yaml"
    angles_out = args.output / "angles.csv"
    a_out = args.output / args.yaml["data"]["a-file"]
    b_out = args.output / args.yaml["data"]["b-file"]
    ffa_out = args.output / args.yaml["data"]["ffa-file"]
    ffb_out = args.output / args.yaml["data"]["ffb-file"]

    # Copy reuseable files

    shutil.copy(conf_in, conf_out)
    shutil.copy(angles_in, angles_out)

    # Write out new data

    elsa.EDF.write(elsa.DataContainerf(a), str(a_out))
    elsa.EDF.write(elsa.DataContainerf(b), str(b_out))
    elsa.EDF.write(elsa.DataContainerf(ffa), str(ffa_out))
    elsa.EDF.write(elsa.DataContainerf(ffb), str(ffb_out))


# 1. load ff[a,b]
# 2. create phantom
# 3. project phantom
# 4. evaluate cosine (phi = 0) }
# 5. apply poisson noise (*I_0)  } Equivalent to Rician noise, but wanna be sure, TODO: do this later!
# 6. preprocess (DFT)		   }
# 7. reconstruct with all models, solvers
# 8. plot L2(eta, eta_hat) vs I_0
# Expectation: Bad fit for very low I_0, as gaussian approx of poisson is not valid there
# Then 2b gets better, followed by 2a, will Rician at some point be like (log)-normal?
