#!/usr/bin/env python3
import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from matplotlib.colors import hsv_to_rgb
from utils import *

import itertools

mplstyle.use('fast')


def direction2color3D(v: np.ndarray) -> np.ndarray:
    """
    Arbirarily map a 3d direction to a color
    """
    x, y, z = v
    hue = (1.01 + np.arctan2(y, x) / np.pi) / 2
    return hsv_to_rgb([hue, (1+z)/2, 1.0])


def visualizeFibers3D(ax: Axes3D, magnitudes: np.ndarray, directionIndices: np.ndarray, samplingDirections: np.ndarray, numFibers: int = 1, zScore: float = .5) -> None:
    """
    Visualize the fibers in 3D
    Parameters
    ----------
    ax : Axes3D
        The axis to plot on
    magnitudes : np.ndarray
        The scattering strengths of the fibers
    directionIndices : np.ndarray
        The indices of the directions of the local maxima
    samplingDirections : np.ndarray
        The directions of the fibers
    numFibers : int
        The number of fibers to plot
    zScore : float
        The number of standard deviations to consider for fiber thresholding
    """

    _, sz, sy, sx = magnitudes.shape

    for i in range(numFibers):

        # take the ith maximum
        r_i = magnitudes[i, :, :, :]
        idx_i = directionIndices[i, :, :, :]

        # Arbitrarily choose a few sigmas in magnitude as threshold

        sigma_r = np.std(magnitudes)
        threshold = np.mean(magnitudes) + zScore * sigma_r

        for ix, iy, iz in itertools.product(range(0, sx), range(0, sy), range(0, sz)):

            radius = r_i[iz, iy, ix]

            if radius > threshold:
                direction = samplingDirections[idx_i[iz, iy, ix]]

                dx, dy, dz = direction * radius / np.max(magnitudes)

                # Confine to upper half sphere as eta(-d)=eta(d)
                x, y, z = direction
                if z < 0 or (z == 0 and y < 0) or (z == 0 and z == 0 and x < 0):
                    direction = - direction

                col = direction2color3D(direction)
                ax.plot([ix - dx, ix + dx],
                        [iy - dy, iy + dy],
                        [iz - dz, iz + dz],
                        '-', color=col)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='AXDT 3D Fibers Visualization')

    parser.add_argument("--eta", "-c", required=True, type=Path,
                        help="Dark field signal coefficients to visualize")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--interactive", "-i", action="store_true",
                        help="Show interactive plot")

    parser.add_argument("--every", "-e", type=int, default=1,
                        help="Plot fibers for every nth voxel")

    parser.add_argument("--num", "-n", type=int, default=1,
                        help="Number of local maxima to plot")

    parser.add_argument("--max-l", "-l", type=int, default=4,
                        help="Truncate degrees above this l")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    parser.add_argument("--phi", type=int, default=30,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=15,
                        help="Resolution in theta")

    parser.add_argument("--no-clip", action="store_true",
                        help="Don't clip scattering strength before Funk-Radon transform")

    args = parser.parse_args()

    if args.output is None and not args.interactive:
        print("Need to supply output directory or --interactive!")
        sys.exit(1)

    # Load the spherical coefficients
    eta = np.load(args.eta)

    eta = eta[:, ::args.every, ::args.every, ::args.every]

    # Extract fibers using a C++ helper function
    magnitudes, indices, directions = extractFibers(
        eta, args.num, (args.theta, args.phi), args.max_l, not args.no_clip)

    fig = plt.figure(figsize=(10, 10), constrained_layout=True)

    ax = fig.add_subplot(projection="3d")

    # Start in a sensible camera position
    ax.view_init(elev=90, azim=90, roll=0)

    visualizeFibers3D(ax, magnitudes, indices,
                      directions, args.num, args.zscore)

    # Keep equal aspect ratio to not distort the view
    set_axes_equal(ax)
    ax.set_box_aspect([1, 1, 1])

    fig.suptitle(f"AXDT fiber visualization - {args.eta}")
    if args.output is not None:
        fig.savefig(args.output)

    if args.interactive:
        plt.show()
