#!/usr/bin/env python3
import argparse
import itertools
from pathlib import Path
from typing import List, Optional, Tuple

import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider
from utils import *

import pyelsa as elsa


def direction2color(direction: np.ndarray) -> np.ndarray:
    """
    Arbirarily map a 2d direction to a color
    """
    samples = np.array([
        [0.8, 0, 1, 0, 0, 2],
        [0, 0.8, 0, 0, 1, 2],
        [0.3, 0.3, 0, 1, 0, 1.8],
    ])

    vn = direction / (np.linalg.norm(direction) + np.finfo(float).eps)
    n = samples.shape[0]

    # run through the samples, accumulate, and normalize afterward
    rgb = np.zeros(3)
    for i in range(n):
        sv = samples[i, 0:2]
        sv = sv / np.linalg.norm(sv)
        sc = samples[i, 2:5]
        sc = sc / max(sc)
        sf = samples[i, 5]

        # compute angle between sample and query vector
        factor = np.arcsin(np.abs(np.dot(vn, sv))) / (np.pi / 2) * sf
        rgb = rgb + factor * sc

    rgb = rgb / (max(rgb) + np.finfo(float).eps)
    return rgb


def visualizeFibers2D(ax: matplotlib.axes.Axes, magnitudes: np.ndarray, indices: np.ndarray, samplingDirections: np.ndarray, attenuation: Optional[np.ndarray], zScore: float, zIndex: int) -> None:
    """
    Visualize the fibers (and optionally attenuation) in a 2D slice of the volume
    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The axis to plot on
    magnitudes : np.ndarray
        The scattering strengths of the fibers
    indices : np.ndarray
        The indices of the directions of the local maxima
    samplingDirections : np.ndarray
        The directions of the fibers
    attenuation : Optional[np.ndarray]
        The attenuation density to plot as background
    zScore : float
        Number of standard deviations to consider for fiber thresholding
    zIndex : int
        The slice to visualize

    """

    if attenuation is not None:
        ax.imshow(attenuation[zIndex, :, :], cmap="gray")

    _, _, sy, sx = magnitudes.shape

    # Take slice at height iz
    mag = magnitudes[:, zIndex, :, :]
    idx = indices[:, zIndex, :, :]

    numFibers = magnitudes.shape[0]

    for i in range(numFibers):

        # Take the ith maximum
        r = mag[i, :, :]
        ind = idx[i, :, :]

        # Arbitrarily choose a few sigmas in magnitude as threshold
        sigma_r = np.std(magnitudes)
        threshold = np.mean(magnitudes) + zScore * sigma_r

        for ix, iy in itertools.product(range(0, sx), range(0, sy)):

            radius = r[iy, ix]

            if radius > threshold:
                # Project into xy plane
                direction = samplingDirections[ind[iy, ix]][:2]

                col = direction2color(direction)

                # Roughly scale length according to magnitude
                dx, dy = direction * \
                    (1.2 - ((2 * radius) / np.max(magnitudes)))

                ax.plot([ix - dx, ix + dx],
                        [iy - dy, iy + dy],
                        '-', color=col)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT 2D Fiber Visualization')

    parser.add_argument("--eta", "-c", required=True, type=Path,
                        help="Dark field signal coefficients to visualize")

    parser.add_argument("--mu", "-m", type=Path,
                        help="Attenuation density as background")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--interactive", "-i", action="store_true",
                        help="Show interactive plot")

    parser.add_argument("--max-l", "-l", type=int, default=4,
                        help="Truncate degrees above this l")

    parser.add_argument("--num", "-n", type=int, default=1,
                        help="Number of local maxima/fibers to plot")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    parser.add_argument("--phi", type=int, default=20,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=20,
                        help="Resolution in theta")

    parser.add_argument("--no-clip", action="store_true",
                        help="Don't clip scattering strength before Funk-Radon transform")

    args = parser.parse_args()

    if args.output is None and not args.interactive:
        print("Need to supply output directory or --interactive!")
        sys.exit(1)

    # Load the reconstruction to visualize
    eta = np.load(args.eta)

    if args.mu:
        mu = np.load(args.mu)
    else:
        mu = None

    # Extract fibers using a C++ helper function
    magnitudes, indices, directions = extractFibers(
        eta, args.num, (args.theta, args.phi), args.max_l, not args.no_clip)

    fig = plt.figure(figsize=(10, 10))

    ax = fig.add_subplot()
    ax.set_xlim(0, eta.shape[1])
    ax.set_ylim(0, eta.shape[2])
    fig.suptitle(f"AXDT fiber visualization - {args.eta}")

    # Initially plot the slice at z=0
    visualizeFibers2D(ax, magnitudes, indices, directions, mu, args.zscore, 0)

    # Optionally save the plot to disk
    if args.output is not None:
        fig.savefig(args.output)

    if args.interactive:

        height = eta.shape[1]

        fig.subplots_adjust(bottom=0.25)
        ax_i = fig.add_axes([0.25, 0.1, 0.65, 0.03])

        allowed_indices = range(height)

        slider = Slider(
            ax_i, "z", 0, height-1,
            valinit=0, valstep=allowed_indices,
            color="green"
        )

        # Allow the user to interactively change the slice
        def update(i):
            ax.clear()
            visualizeFibers2D(ax, magnitudes, indices,
                              directions, mu, args.zscore, i)
            ax.set_xlim(0, eta.shape[1])
            ax.set_ylim(0, eta.shape[2])
            fig.canvas.draw_idle()

        slider.on_changed(update)

        plt.show()
    else:
        plt.close(fig)
