#!/usr/bin/env python3
from icosphere import icosphere
import argparse
from pathlib import Path
import re
from typing import List, Optional, Tuple

from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
import mpl_toolkits
import tqdm

from utils import *

import pyelsa as elsa


numWeights = np.load("oldNum.npy")
anaWeights = np.load("oldAna.npy")

numWeights /= np.max(numWeights)
anaWeights /= np.max(anaWeights)

diff = numWeights - anaWeights


fig, axs = plt.subplots(nrows=15, ncols=1, sharex=True,
                        sharey=False, constrained_layout=True)
for i in range(15):
    axs[i].plot(numWeights[i])
    axs[i].plot(anaWeights[i])

    axs[i].plot(diff[i])


plt.show()

# print(f"min={np.min(diff):.3g}", f"max={np.max(diff):.3g}",
#       f"{np.mean(diff):.3g}±{np.std(diff):.3g}")

# print(np.correlate(numWeights[1], anaWeights[1]))

# # numWeights = numWeights.reshape((60, 300))
# # anaWeights = anaWeights.reshape((60, 300))

# fig, axs = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=True)

# axs[0].imshow(numWeights)
# axs[1].imshow(anaWeights)
# axs[2].imshow(numWeights - anaWeights)
# plt.show()
