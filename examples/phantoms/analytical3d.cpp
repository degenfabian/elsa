#include "VolumeDescriptor.h"
#include "AnalyticalSinogram.h"
#include "CircleTrajectoryGenerator.h"
#include "JosephsMethod.h"
#include "IO.h"
#include "SIRT.h"

using namespace elsa;
using namespace elsa::phantoms;

int main(int, char*[])
{

    IndexVector_t size({{100, 100, 100}});
    VolumeDescriptor image{size};

    index_t numAngles{360}, arc{360};
    const auto distance = 100.0;
    auto sinoDescriptor = CircleTrajectoryGenerator::createTrajectory(numAngles, image, arc,
                                                                      distance * 100.0f, distance);

    Logger::get("Info")->info("Making sinogram...");
    auto analyticalSinogram = analyticalSheppLogan<float>(image, *sinoDescriptor);

    Logger::get("Info")->info("Writing out sinogram...");
    io::write(analyticalSinogram, "3dsinogram.edf");

    JosephsMethod projector{image, *sinoDescriptor};

    // solve the reconstruction problem
    SIRT solver(projector, analyticalSinogram);

    index_t noIterations{50};
    Logger::get("Info")->info("Solving reconstruction using {} iterations", noIterations);
    auto reconstruction = solver.solve(noIterations);

    // write the reconstruction out
    Logger::get("Info")->info("Writing out reconstruction...");
    io::write(reconstruction, "3dreconstruction.edf");
    for (index_t i = 0; i < size[2]; i++) {
        std::ostringstream o;
        o << "slices/slice" << i << ".pgm";
        io::write(reconstruction.slice(i), o.str());
    }

    return 0;
}
