
#include <doctest/doctest.h>

#include "DataContainer.h"
#include "Scaling.h"
#include "testHelpers.h"
#include "ExpLeastSquares.h"
#include "Identity.h"
#include "VolumeDescriptor.h"
#include "TypeCasts.hpp"

using namespace elsa;
using namespace doctest;

TYPE_TO_STRING(complex<float>);
TYPE_TO_STRING(complex<double>);

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("ExpLeastSquares: with Identity operator", data_t, float, double)
{
    srand((unsigned int) 666);

    using Vector = Eigen::Matrix<data_t, Eigen::Dynamic, 1>;

    VolumeDescriptor dd({7, 10});
    const auto size = dd.getNumberOfCoefficients();

    auto id = Identity<data_t>(dd);
    auto b = DataContainer<data_t>(dd, Vector_t<data_t>::Random(size));

    ExpLeastSquares<data_t> ls(id, b);

    THEN("the descriptor is as expected")
    {
        CHECK_EQ(ls.getDomainDescriptor(), dd);
    }

    THEN("Clone works as expected")
    {
        auto clone = ls.clone();

        CHECK_NE(clone.get(), &ls);
        CHECK_EQ(*clone, ls);
    }

    THEN("it evaluates to 0.5 * ||exp(-x) - b||_2^2")
    {
        auto x = DataContainer<data_t>(dd, Vector::Random(b.getSize()));

        auto val = ls.evaluate(x);

        CHECK_EQ(val, doctest::Approx(0.5 * (exp(-x) - b).squaredL2Norm()));
    }

    THEN("the gradient is -A^T exp(-Ax) (exp(-Ax) - b)")
    {
        auto x = DataContainer<data_t>(dd, Vector::Random(b.getSize()));

        auto grad = ls.getGradient(x);
        auto expected = -exp(-x) * (exp(-x) - b);

        CHECK_UNARY(isApprox(grad, expected));
    }

    THEN("Hessian is A^T * (exp(-Ax) * (2 * exp(-Ax) - b)) A")
    {
        auto x = DataContainer<data_t>(dd, Vector::Random(b.getSize()));
        auto d = exp(-id.apply(x));

        auto hessian = ls.getHessian(x);
        auto expected = adjoint(id) * (d * (2.f * d - b)) * id;

        CHECK_EQ(hessian, expected);
    }
}
