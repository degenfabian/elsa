#include <cmath>
#include <doctest/doctest.h>
#include <limits>

#include "testHelpers.h"
#include "L1Norm.h"
#include "Identity.h"
#include "VolumeDescriptor.h"
#include "TypeCasts.hpp"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("L1Norm: Testing without residual", data_t, float, double)
{
    using Vector = Eigen::Matrix<data_t, Eigen::Dynamic, 1>;

    GIVEN("just data (no residual)")
    {
        IndexVector_t numCoeff(1);
        numCoeff << 4;
        VolumeDescriptor dd(numCoeff);

        WHEN("instantiating")
        {
            L1Norm<data_t> func(dd);

            THEN("the functional is as expected")
            {
                CHECK_EQ(func.getDomainDescriptor(), dd);
            }

            THEN("a clone behaves as expected")
            {
                auto l1Clone = func.clone();

                CHECK_NE(l1Clone.get(), &func);
                CHECK_EQ(*l1Clone, func);
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector dataVec(dd.getNumberOfCoefficients());
                dataVec << -9, -4, 0, 1;
                DataContainer<data_t> dc(dd, dataVec);

                CHECK(checkApproxEq(func.evaluate(dc), 14));
                CHECK_UNARY(std::isinf(func.convexConjugate(dc)));
                CHECK_THROWS_AS(func.getGradient(dc), LogicError);
                CHECK_THROWS_AS(func.getHessian(dc), LogicError);

                auto proxdual = func.proxdual(dc, 1);
                CHECK_UNARY(isApprox(proxdual, dc - func.proximal(dc, 1)));
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector dataVec(dd.getNumberOfCoefficients());
                dataVec << 0.1, -0.2, 0.3, -0.4;
                DataContainer<data_t> dc(dd, dataVec);

                CHECK(checkApproxEq(func.evaluate(dc), 1.));
                CHECK(checkApproxEq(func.convexConjugate(dc), 0));
                CHECK_THROWS_AS(func.getGradient(dc), LogicError);
                CHECK_THROWS_AS(func.getHessian(dc), LogicError);

                auto proxdual = func.proxdual(dc, 1);
                CHECK_UNARY(isApprox(proxdual, dc - func.proximal(dc, 1)));
            }
        }
    }
}

TEST_SUITE_END();
