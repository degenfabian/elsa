#pragma once

#include "DataDescriptor.h"
#include "Functional.h"
#include "DataContainer.h"
#include "LinearOperator.h"

namespace elsa
{
    /**
     * @brief The least squares functional / loss functional with the linear operator
     * in the exponential
     *
     * The exponential least squares loss is given by:
     * \[
     * 0.5 * || \exp(-A(x)) - b ||_2^2
     * \]
     *
     * The gradient is given as
     * \[
     * -A^T \exp(-Ax) (\exp(-Ax) - b)
     * \]
     * and the Hessian:
     * \[
     * A^T \text{diag} \left(\exp(-Ax) (2 \exp(-Ax) - b) \right) B
     * \]
     *
     * This functional is non-linear (due to the exponential) and twice differentiable.
     * It is an important functional to model Gaussian noise of e.g. dark-field
     * signals for AXDT.
     *
     * @tparam data_t data type for the domain of the residual of the functional, defaulting to
     * real_t
     */
    template <typename data_t = real_t>
    class ExpLeastSquares : public Functional<data_t>
    {
    public:
        ExpLeastSquares(const LinearOperator<data_t>& A, const DataContainer<data_t>& b);

        // make copy constructor deletion explicit
        ExpLeastSquares(const ExpLeastSquares<data_t>&) = delete;

        // default destructor
        ~ExpLeastSquares() override = default;

        const LinearOperator<data_t>& getOperator() const;

        const DataContainer<data_t>& getDataVector() const;

        bool isDifferentiable() const override;

    protected:
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /// the computation of the Hessian
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /// implement the polymorphic clone operation
        ExpLeastSquares<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Functional<data_t>& other) const override;

    private:
        std::unique_ptr<LinearOperator<data_t>> A_{};

        DataContainer<data_t> b_{};
    };

} // namespace elsa
