#pragma once

#include "BaseHelixTrajectoryGenerator.h"
#include "CurvedDetectorDescriptor.h"

#include <optional>

namespace elsa
{
    /**
     * @brief Generator for helical trajectories as used in X-ray Computed Tomography
     * (for 3d).
     *
     * @author Fabian Degen - initial code
     */
    class CurvedHelixTrajectoryGenerator : public BaseHelixTrajectoryGenerator
    {
    public:
        /**
         * @brief Generate a list of geometries corresponding to a helical trajectory around a
         * volume captured on a curved detector.
         *
         * @param volumeDescriptor the volume around which the trajectory should go
         * @param thetas array of acquisition angles
         * @param pitch the distance a helix advances along its central axis per one complete turn
         * @param sourceToCenter the distance of the X-ray source to
         * the center of the volume
         * @param centerToDetector the distance of the center of the volume
         * to the X-ray detector
         * @param angle rotation angle (in radians)
         *
         * @returns a pair containing the list of geometries with a helical trajectory, and the
         * sinogram data descriptor
         *
         * Please note: the sinogram size/spacing will match the volume size/spacing.
         */
        static std::unique_ptr<CurvedDetectorDescriptor>
            createTrajectory(const DataDescriptor& volumeDescriptor, std::vector<real_t> thetas,
                             real_t pitch, real_t sourceToCenter, real_t centerToDetector,
                             geometry::Radian angle,
                             std::optional<RealVector_t> principalPointOffset = std::nullopt,
                             std::optional<RealVector_t> centerOfRotOffset = std::nullopt,
                             std::optional<IndexVector_t> detectorSize = std::nullopt,
                             std::optional<RealVector_t> detectorSpacing = std::nullopt);
    };
} // namespace elsa
