#pragma once

#include "DataContainer.h"
#include "DetectorDescriptor.h"
#include "VolumeDescriptor.h"

namespace elsa::phantoms
{
    /**
        * @brief Generates an analytical Shepp-Logan sinogram.
        *
        * This function generates an analytical sinogram using CPU raytracing.
        * The sinogram is generated based on the provided image and sinogram descriptors.
        *
        * @param imageDescriptor The descriptor of the input image.
        * @param sinogramDescriptor The descriptor of the output sinogram.
        * @return The generated sinogram as a DataContainer.

        * @author Cederik Höfs
    */
    template <typename data_t>
    DataContainer<data_t> analyticalSheppLogan(const VolumeDescriptor& imageDescriptor,
                                               const DetectorDescriptor& sinogramDescriptor);

} // namespace elsa::phantoms