/**
 * @file test_HelixTrajectoryGenerator.cpp
 *
 * @brief Test for HelixTrajectoryGenerator class
 *
 * @author Fabian Degen - initial code
 */

#include "doctest/doctest.h"

#include "PlanarHelixTrajectoryGenerator.h"
#include "Logger.h"
#include "VolumeDescriptor.h"
#include "testHelpers.h"

#include <tuple>

using namespace elsa;
using namespace doctest;

TEST_CASE("PlanarHelixTrajectoryGenerator: Create a Helical Trajectory")
{
    using namespace geometry;

    const index_t s = 64;

    // Detector size is the volume size scalled by the square root of 2
    const auto expectedDetectorSize = static_cast<index_t>(s * std::sqrt(2));

    GIVEN("A 2D descriptor and 256 angles")
    {
        std::vector<real_t> thetas = [&]() {
            auto tmp = RealVector_t::LinSpaced(256, 0, static_cast<real_t>(180));
            return std::vector<real_t>{tmp.begin(), tmp.end()};
        }();

        real_t pitch = 5;
        IndexVector_t volSize(2);
        volSize << s, s;
        const VolumeDescriptor desc{volSize};

        WHEN("Trying to create a 2D helical trajectory")
        {
            real_t diffCenterSource{s * 100};
            real_t diffCenterDetector{s};

            REQUIRE_THROWS_AS(PlanarHelixTrajectoryGenerator::createTrajectory(
                                  desc, thetas, pitch, diffCenterSource, diffCenterDetector),
                              InvalidArgumentError);
        }
    }

    GIVEN("A 3D descriptor and 256 angles")
    {
        std::vector<real_t> thetas = [&]() {
            auto tmp = RealVector_t::LinSpaced(256, 0, static_cast<real_t>(720));
            return std::vector<real_t>{tmp.begin(), tmp.end()};
        }();

        IndexVector_t volSize(3);
        volSize << s, s, s;
        VolumeDescriptor desc{volSize};

        WHEN("We create a helical trajectory with two full turns and different pitches for this "
             "scenario")
        {
            real_t diffCenterSource{s * 100};
            real_t diffCenterDetector{s};
            real_t numberOfRotations{2};

            std::vector<real_t> pitches{0.5, 1, 5, 10, 20, 50, 100};

            // iterate over pitch values
            for (const auto& pitch : pitches) {
                SUBCASE(("Testing with pitch value: " + std::to_string(pitch)).c_str())
                {

                    // calculate x, y, z coordinates that the camera should have
                    std::vector<std::tuple<index_t, index_t, index_t>> camera_positions;
                    camera_positions.reserve(256);

                    std::vector<real_t> lambdas = [&]() {
                        auto tmp = RealVector_t::LinSpaced(256, 0, numberOfRotations * pitch);
                        return std::vector<real_t>{tmp.begin(), tmp.end()};
                    }();

                    // iterating from lambda_min to lambda_max
                    for (real_t lambda : lambdas) {
                        camera_positions.emplace_back(
                            std::make_tuple(diffCenterSource * sin(2 * M_PI * lambda / pitch)
                                                + diffCenterDetector / 2,
                                            lambda + diffCenterDetector / 2,
                                            diffCenterSource * cos(2 * M_PI * lambda / pitch + M_PI)
                                                + diffCenterDetector / 2));
                    }

                    THEN("Every camera position of the helical trajectory matches with the "
                         "corresponding expected x, y, z coordinates")
                    {
                        auto sdesc = PlanarHelixTrajectoryGenerator::createTrajectory(
                            desc, thetas, pitch, diffCenterSource, diffCenterDetector);

                        auto geometries = sdesc->getGeometry();

                        // compare actual camera center coordinates with expected ones
                        for (int i{0}; i < 256; i++) {
                            REQUIRE(checkApproxEq(geometries[i].getCameraCenter()[0],
                                                  std::get<0>(camera_positions[i]), 0.9));
                            REQUIRE(checkApproxEq(geometries[i].getCameraCenter()[1],
                                                  std::get<1>(camera_positions[i]), 0.9));
                            REQUIRE(checkApproxEq(geometries[i].getCameraCenter()[2],
                                                  std::get<2>(camera_positions[i]), 0.9));
                        }
                    }
                }
            }
        }

        WHEN("We create a helical trajectory with pitch of 5 and different number of rotations "
             "for this scenario")
        {
            real_t diffCenterSource{s * 100};
            real_t diffCenterDetector{s};
            real_t pitch{5};

            std::vector<real_t> numberOfRotations{0.5, 1, 5, 10, 20, 50, 100};

            // iterate over numberOfRotations values
            for (const auto& numRotations : numberOfRotations) {
                SUBCASE(("Testing with " + std::to_string(numRotations) + " number of rotations")
                            .c_str())
                {
                    // calculate x, y, z coordinates that the camera should have
                    std::vector<std::tuple<index_t, index_t, index_t>> camera_positions;
                    camera_positions.reserve(256);

                    std::vector<real_t> lambdas = [&]() {
                        auto tmp = RealVector_t::LinSpaced(256, 0, numRotations * pitch);
                        return std::vector<real_t>{tmp.begin(), tmp.end()};
                    }();

                    // iterating from lambda_min to lambda_max
                    for (real_t lambda : lambdas) {
                        camera_positions.emplace_back(
                            std::make_tuple(diffCenterSource * sin(2 * M_PI * lambda / pitch)
                                                + diffCenterDetector / 2,
                                            lambda + diffCenterDetector / 2,
                                            diffCenterSource * cos(2 * M_PI * lambda / pitch + M_PI)
                                                + diffCenterDetector / 2));
                    }

                    THEN("Every camera position of the helical trajectory matches with the "
                         "corresponding expected x, y, z coordinates")
                    {
                        auto sdesc = PlanarHelixTrajectoryGenerator::createTrajectory(
                            desc, thetas, pitch, diffCenterSource, diffCenterDetector);

                        auto geometries = sdesc->getGeometry();

                        // compare actual camera center coordinates with expected ones
                        for (int i{0}; i < 256; i++) {
                            REQUIRE(checkApproxEq(geometries[i].getCameraCenter()[0],
                                                  std::get<0>(camera_positions[i]), 2));
                            REQUIRE(checkApproxEq(geometries[i].getCameraCenter()[1],
                                                  std::get<1>(camera_positions[i]), 2));
                            REQUIRE(checkApproxEq(geometries[i].getCameraCenter()[2],
                                                  std::get<2>(camera_positions[i]), 2));
                        }
                    }
                }
            }
        }
    }
}