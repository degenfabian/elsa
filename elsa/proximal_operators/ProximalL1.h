#pragma once

#include "DataContainer.h"
#include "StrongTypes.h"
#include "elsaDefines.h"

namespace elsa
{
    /**
     * @brief Class representing the proximal operator of the l1 norm
     *
     * @author Andi Braimllari - initial code
     *
     * @tparam data_t data type for the values of the operator, defaulting to real_t
     *
     * This class represents the soft thresholding operator, expressed by its apply method through
     * the function i.e. @f$ prox(v) = sign(v)·(|v| - t)_+. @f$
     *
     * References:
     * http://sfb649.wiwi.hu-berlin.de/fedc_homepage/xplore/tutorials/xlghtmlnode93.html
     */
    template <typename data_t = real_t>
    class ProximalL1
    {
    public:
        ProximalL1() = default;

        explicit ProximalL1(data_t sigma);

        explicit ProximalL1(const DataContainer<data_t>& b);

        ProximalL1(const DataContainer<data_t>& b, SelfType_t<data_t> sigma);

        // default copy constructor
        ProximalL1(const ProximalL1<data_t>&) = default;

        // default copy assignment
        ProximalL1& operator=(const ProximalL1<data_t>&) = default;

        // move constructor
        ProximalL1(ProximalL1<data_t>&&) noexcept;

        // move assignment
        ProximalL1& operator=(ProximalL1<data_t>&&) noexcept;

        /// default destructor
        ~ProximalL1() = default;

        /**
         * @brief apply the proximal operator of the l1 norm to an element in the operator's domain
         *
         * @param[in] v input DataContainer
         * @param[in] t input Threshold
         * @param[out] prox output DataContainer
         */
        void apply(const DataContainer<data_t>& v, SelfType_t<data_t> t,
                   DataContainer<data_t>& prox) const;

        DataContainer<data_t> apply(const DataContainer<data_t>& v, SelfType_t<data_t> t) const;

    private:
        data_t sigma_{1};

        std::optional<DataContainer<data_t>> b_ = {};
    };

    template <class data_t>
    using SoftThresholding = ProximalL1<data_t>;
} // namespace elsa
