#include <cmath>
#include <optional>

#include "ProximalL1.h"
#include "DataContainer.h"
#include "Error.h"
#include "TypeCasts.hpp"
#include "Math.hpp"
#include "elsaDefines.h"

namespace elsa
{
    template <class data_t>
    DataContainer<data_t> softthreshold(const DataContainer<data_t>& x, SelfType_t<data_t> thresh)
    {
        return elsa::maximum(elsa::cwiseAbs(x) - thresh, 0) * sign(x);
    }

    template <typename data_t>
    ProximalL1<data_t>::ProximalL1(data_t sigma) : sigma_(sigma), b_(std::nullopt)
    {
    }

    template <class data_t>
    ProximalL1<data_t>::ProximalL1(const DataContainer<data_t>& b) : sigma_(1.0), b_(b)
    {
    }

    template <class data_t>
    ProximalL1<data_t>::ProximalL1(const DataContainer<data_t>& b, SelfType_t<data_t> sigma)
        : sigma_(sigma), b_(b)
    {
    }

    template <class data_t>
    ProximalL1<data_t>::ProximalL1(ProximalL1<data_t>&& other) noexcept
        : sigma_(other.sigma_), b_(std::move(other.b_))
    {
    }

    template <class data_t>
    ProximalL1<data_t>& ProximalL1<data_t>::operator=(ProximalL1<data_t>&& other) noexcept
    {
        sigma_ = other.sigma_;
        b_ = std::move(other.b_);

        return *this;
    }

    template <typename data_t>
    DataContainer<data_t> ProximalL1<data_t>::apply(const DataContainer<data_t>& v,
                                                    SelfType_t<data_t> t) const
    {
        DataContainer<data_t> out{v.getDataDescriptor()};
        apply(v, t, out);
        return out;
    }

    template <typename data_t>
    void ProximalL1<data_t>::apply(const DataContainer<data_t>& v, SelfType_t<data_t> t,
                                   DataContainer<data_t>& prox) const
    {
        if (v.getSize() != prox.getSize()) {
            throw LogicError("ProximalL1: sizes of v and prox must match");
        }

        if (b_.has_value()) {
            prox.assign(softthreshold(v - *b_, t * sigma_) + *b_);
        } else {
            prox.assign(softthreshold(v, t * sigma_));
        }
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ProximalL1<float>;
    template class ProximalL1<double>;
} // namespace elsa
