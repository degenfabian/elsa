#include "FGM.h"
#include "DataContainer.h"
#include "Error.h"
#include "Functional.h"
#include "Logger.h"
#include "TypeCasts.hpp"
#include "PowerIterations.h"
#include "FixedStepSize.h"

namespace elsa
{
    template <typename data_t>
    FGM<data_t>::FGM(const Functional<data_t>& problem, data_t epsilon)
        : Solver<data_t>(),
          _problem{problem.clone()},
          _epsilon{epsilon},
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}

    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("FGM: Given problem is not differentiable!");
        }
        this->name_ = "FGM";
    }

    template <typename data_t>
    FGM<data_t>::FGM(const Functional<data_t>& problem,
                     const LinearOperator<data_t>& preconditionerInverse, data_t epsilon)
        : Solver<data_t>(),
          _problem{problem.clone()},
          _epsilon{epsilon},
          _preconditionerInverse{preconditionerInverse.clone()},
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("FGM: Given problem is not differentiable!");
        }

        // check that preconditioner is compatible with problem
        if (_preconditionerInverse->getDomainDescriptor().getNumberOfCoefficients()
                != _problem->getDomainDescriptor().getNumberOfCoefficients()
            || _preconditionerInverse->getRangeDescriptor().getNumberOfCoefficients()
                   != _problem->getDomainDescriptor().getNumberOfCoefficients()) {
            throw InvalidArgumentError("FGM: incorrect size of preconditioner");
        }
        this->name_ = "FGM";
    }
    template <typename data_t>
    FGM<data_t>::FGM(const Functional<data_t>& problem,
                     const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : Solver<data_t>(),
          _problem{problem.clone()},
          _epsilon{epsilon},
          _lineSearchMethod{lineSearchMethod.clone()},
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("FGM: Given problem is not differentiable!");
        }
        this->name_ = "FGM";
    }

    template <typename data_t>
    FGM<data_t>::FGM(const Functional<data_t>& problem,
                     const LinearOperator<data_t>& preconditionerInverse,
                     const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : Solver<data_t>(),
          _problem{problem.clone()},
          _epsilon{epsilon},
          _preconditionerInverse{preconditionerInverse.clone()},
          _lineSearchMethod{lineSearchMethod.clone()},
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        // check that preconditioner is compatible with problem
        if (_preconditionerInverse->getDomainDescriptor().getNumberOfCoefficients()
                != _problem->getDomainDescriptor().getNumberOfCoefficients()
            || _preconditionerInverse->getRangeDescriptor().getNumberOfCoefficients()
                   != _problem->getDomainDescriptor().getNumberOfCoefficients()) {
            throw InvalidArgumentError("FGM: incorrect size of preconditioner");
        }
        this->name_ = "FGM";
    }
    template <typename data_t>
    bool FGM<data_t>::shouldStop() const
    {
        return gradient.squaredL2Norm() <= _epsilon * _epsilon * deltaZero;
    }

    template <typename data_t>
    DataContainer<data_t> FGM<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, _problem->getDomainDescriptor());

        this->thetaOld = static_cast<data_t>(1.0);
        this->yOld = x;
        this->gradient = _problem->getGradient(x);
        this->deltaZero = this->gradient.squaredL2Norm();

        if (!_lineSearchMethod) {
            auto L = powerIterations(_problem->getHessian(x), 5);
            this->_lineSearchMethod = std::make_unique<FixedStepSize<data_t>>(*_problem, 1 / L);
            Logger::get("FGM")->info("Starting optimization with lipschitz constant {}", L);
        } else {
            Logger::get("FGM")->info("Starting optimization with a lineSearchMethod");
        }
        this->configured_ = true;
        return x;
    }

    template <typename data_t>
    DataContainer<data_t> FGM<data_t>::step(DataContainer<data_t> x)
    {

        this->gradient = _problem->getGradient(x);

        if (_preconditionerInverse)
            this->gradient = _preconditionerInverse->apply(this->gradient);

        auto alpha = _lineSearchMethod->solve(x, -this->gradient);

        auto y = emptylike(x);
        lincomb(1, x, -alpha, gradient, y);

        const auto theta =
            (data_t{1} + std::sqrt(data_t{1} + data_t{4} * thetaOld * thetaOld)) / data_t{2};

        lincomb(1, y, (thetaOld - data_t{1}) / theta, (y - yOld), x);

        this->thetaOld = theta;
        this->yOld = y;

        return x;
    }

    template <typename data_t>
    std::string FGM<data_t>::formatHeader() const
    {
        return fmt::format("| {:^13} | {:^13} |", "objective", "gradient");
    }

    template <typename data_t>
    std::string FGM<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        auto eval = _problem->evaluate(x);
        auto gradient = _problem->getGradient(x);

        return fmt::format("| {:>13} | {:>13} |", eval, gradient.squaredL2Norm());
    }

    template <typename data_t>
    FGM<data_t>* FGM<data_t>::cloneImpl() const
    {
        if (_lineSearchMethod and _preconditionerInverse) {
            return new FGM(*_problem, *_preconditionerInverse, *_lineSearchMethod, _epsilon);
        } else if (_preconditionerInverse) {
            return new FGM(*_problem, *_preconditionerInverse, _epsilon);
        } else if (_lineSearchMethod) {
            return new FGM(*_problem, *_lineSearchMethod, _epsilon);
        }

        return new FGM(*_problem, _epsilon);
    }

    template <typename data_t>
    bool FGM<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherFGM = downcast_safe<FGM>(&other);
        if (!otherFGM)
            return false;

        if (_epsilon != otherFGM->_epsilon)
            return false;

        if ((_preconditionerInverse && !otherFGM->_preconditionerInverse)
            || (!_preconditionerInverse && otherFGM->_preconditionerInverse))
            return false;

        if (_preconditionerInverse && otherFGM->_preconditionerInverse)
            if (*_preconditionerInverse != *otherFGM->_preconditionerInverse)
                return false;

        if ((_lineSearchMethod and not otherFGM->_lineSearchMethod)
            or (not _lineSearchMethod and otherFGM->_lineSearchMethod))
            return false;

        if (_lineSearchMethod and otherFGM->_lineSearchMethod)
            if (not _lineSearchMethod->isEqual(*(otherFGM->_lineSearchMethod)))
                return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class FGM<float>;
    template class FGM<double>;

} // namespace elsa
