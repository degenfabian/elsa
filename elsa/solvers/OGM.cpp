#include "OGM.h"
#include "DataContainer.h"
#include "Functional.h"
#include "TypeCasts.hpp"
#include "Logger.h"
#include "PowerIterations.h"
#include "FixedStepSize.h"

namespace elsa
{
    template <typename data_t>
    OGM<data_t>::OGM(const Functional<data_t>& problem, data_t epsilon)
        : Solver<data_t>(),
          _problem(problem.clone()),
          _epsilon{epsilon},
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("OGM: Given problem is not differentiable!");
        }
        this->name_ = "OGM";
    }

    template <typename data_t>
    OGM<data_t>::OGM(const Functional<data_t>& problem,
                     const LinearOperator<data_t>& preconditionerInverse, data_t epsilon)
        : Solver<data_t>(),
          _problem(problem.clone()),
          _epsilon{epsilon},
          _preconditionerInverse{preconditionerInverse.clone()},
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("OGM: Given problem is not differentiable!");
        }

        // check that preconditioner is compatible with problem
        if (_preconditionerInverse->getDomainDescriptor().getNumberOfCoefficients()
                != _problem->getDomainDescriptor().getNumberOfCoefficients()
            || _preconditionerInverse->getRangeDescriptor().getNumberOfCoefficients()
                   != _problem->getDomainDescriptor().getNumberOfCoefficients()) {
            throw InvalidArgumentError("OGM: incorrect size of preconditioner");
        }
        this->name_ = "OGM";
    }
    template <typename data_t>
    OGM<data_t>::OGM(const Functional<data_t>& problem,
                     const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : Solver<data_t>(),
          _problem(problem.clone()),
          _epsilon{epsilon},
          _lineSearchMethod(lineSearchMethod.clone()),
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("OGM: Given problem is not differentiable!");
        }
        this->name_ = "OGM";
    }

    template <typename data_t>
    OGM<data_t>::OGM(const Functional<data_t>& problem,
                     const LinearOperator<data_t>& preconditionerInverse,
                     const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : Solver<data_t>(),
          _problem(problem.clone()),
          _epsilon{epsilon},
          _preconditionerInverse{preconditionerInverse.clone()},
          _lineSearchMethod(lineSearchMethod.clone()),
          yOld{empty<data_t>(problem.getDomainDescriptor())},
          gradient{emptylike(yOld)}
    {
        if (!problem.isDifferentiable()) {
            throw InvalidArgumentError("OGM: Given problem is not differentiable!");
        }

        // check that preconditioner is compatible with problem
        if (_preconditionerInverse->getDomainDescriptor().getNumberOfCoefficients()
                != _problem->getDomainDescriptor().getNumberOfCoefficients()
            || _preconditionerInverse->getRangeDescriptor().getNumberOfCoefficients()
                   != _problem->getDomainDescriptor().getNumberOfCoefficients()) {
            throw InvalidArgumentError("OGM: incorrect size of preconditioner");
        }
        this->name_ = "OGM";
    }

    template <typename data_t>
    bool OGM<data_t>::shouldStop() const
    {
        return this->gradient.squaredL2Norm() <= _epsilon * _epsilon * deltaZero;
    }

    template <typename data_t>
    DataContainer<data_t> OGM<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        this->thetaOld = data_t{1};
        auto x = extract_or(x0, _problem->getDomainDescriptor());
        this->yOld = x;
        this->gradient = _problem->getGradient(x);
        this->deltaZero = this->gradient.squaredL2Norm();

        if (!_lineSearchMethod) {
            // OGM is very picky when it comes to the accuracy of the used lipschitz constant
            // therefore we use 20 power iterations instead of 5 here to be more precise. In some
            // cases OGM might still not converge then an even more precise constant is needed
            auto L = powerIterations(_problem->getHessian(x), 20);
            this->_lineSearchMethod = std::make_unique<FixedStepSize<data_t>>(*_problem, 1 / L);

            Logger::get("OGM")->info("Starting optimization with lipschitz constant {}", L);
        } else {
            Logger::get("OGM")->info("Starting optimization with a lineSearchMethod");
        }

        this->configured_ = true;

        return x;
    }

    template <typename data_t>
    std::string OGM<data_t>::formatHeader() const
    {
        return fmt::format("| {:^13} | {:^13} |", "objective", "gradient");
    }

    template <typename data_t>
    std::string OGM<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        auto eval = _problem->evaluate(x);
        auto gradient = _problem->getGradient(x);

        return fmt::format("| {:>13} | {:>13} |", eval, gradient.squaredL2Norm());
    }

    template <typename data_t>
    DataContainer<data_t> OGM<data_t>::step(DataContainer<data_t> x)
    {

        auto y = emptylike(x);

        this->_problem->getGradient(x, this->gradient);

        if (this->_preconditionerInverse)
            this->_preconditionerInverse->apply(this->gradient, this->gradient);

        auto alpha = this->_lineSearchMethod->solve(x, -this->gradient);

        lincomb(1, x, -alpha, this->gradient, y);

        // TODO: Consult David on how to handle this in the steppable interface
        const auto theta =
            data_t{0.5} * (data_t{1} + std::sqrt(data_t{1} + data_t{4} * std::pow(thetaOld, 2)));

        // x_{i+1} = y_{i+1} + \frac{\theta_i-1}{\theta_{i+1}}(y_{i+1} - y_i) +
        // \frac{\theta_i}{\theta_{i+1}}/(y_{i+1} - x_i)
        lincomb(1, y, ((this->thetaOld - data_t{1}) / theta), (y - this->yOld), x);
        lincomb(1, x,
                -(this->thetaOld / theta) * this->_lineSearchMethod->solve(x, -this->gradient),
                this->gradient, x);

        this->thetaOld = theta;
        this->yOld = y;

        return x;
    }

    template <typename data_t>
    OGM<data_t>* OGM<data_t>::cloneImpl() const
    {
        if (_lineSearchMethod and _preconditionerInverse) {
            return new OGM(*_problem, *_preconditionerInverse, *_lineSearchMethod, _epsilon);
        } else if (_preconditionerInverse) {
            return new OGM(*_problem, *_preconditionerInverse, _epsilon);
        } else if (_lineSearchMethod) {
            return new OGM(*_problem, *_lineSearchMethod, _epsilon);
        }
        return new OGM(*_problem, _epsilon);
    }

    template <typename data_t>
    bool OGM<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherOGM = downcast_safe<OGM>(&other);
        if (!otherOGM)
            return false;

        if (_epsilon != otherOGM->_epsilon)
            return false;

        if ((_preconditionerInverse && !otherOGM->_preconditionerInverse)
            || (!_preconditionerInverse && otherOGM->_preconditionerInverse))
            return false;

        if (_preconditionerInverse && otherOGM->_preconditionerInverse)
            if (*_preconditionerInverse != *otherOGM->_preconditionerInverse)
                return false;

        if ((_lineSearchMethod and not otherOGM->_lineSearchMethod)
            or (not _lineSearchMethod and otherOGM->_lineSearchMethod))
            return false;

        if (_lineSearchMethod and otherOGM->_lineSearchMethod)
            if (not _lineSearchMethod->isEqual(*(otherOGM->_lineSearchMethod)))
                return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class OGM<float>;
    template class OGM<double>;

} // namespace elsa
