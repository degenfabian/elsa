#include "GradientDescent.h"
#include "Functional.h"
#include "Logger.h"
#include "TypeCasts.hpp"
#include "PowerIterations.h"
#include <iostream>

namespace elsa
{

    template <typename data_t>
    GradientDescent<data_t>::GradientDescent(const Functional<data_t>& problem, data_t stepSize)
        : Solver<data_t>(),
          _problem(problem.clone()),
          _lineSearchMethod(FixedStepSize<data_t>(*_problem, stepSize).clone())
    {
    }

    template <typename data_t>
    GradientDescent<data_t>::GradientDescent(const Functional<data_t>& problem)
        : Solver<data_t>(), _problem(problem.clone())
    {
    }

    template <typename data_t>
    GradientDescent<data_t>::GradientDescent(const Functional<data_t>& problem,
                                             const LineSearchMethod<data_t>& lineSearchMethod)
        : Solver<data_t>(), _problem(problem.clone()), _lineSearchMethod(lineSearchMethod.clone())
    {
    }

    template <typename data_t>
    DataContainer<data_t> GradientDescent<data_t>::solve(index_t iterations,
                                                         std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, _problem->getDomainDescriptor());

        // If stepSize is not initialized yet, we do it know with x0
        if (!_lineSearchMethod) {
            _lineSearchMethod =
                FixedStepSize<data_t>(*_problem, powerIterations(_problem->getHessian(x))).clone();
            Logger::get("GradientDescent")
                ->info("Step length is chosen to be: {:8.5})", _lineSearchMethod->solve(x, x));
        }

        for (index_t i = 0; i < iterations; ++i) {
            Logger::get("GradientDescent")->info("iteration {} of {}", i + 1, iterations);
            auto gradient = _problem->getGradient(x);
            x -= _lineSearchMethod->solve(x, -gradient) * gradient;
        }

        return x;
    }

    template <typename data_t>
    GradientDescent<data_t>* GradientDescent<data_t>::cloneImpl() const
    {
        if (_lineSearchMethod) {
            return new GradientDescent(*_problem, *_lineSearchMethod);
        } else {
            return new GradientDescent(*_problem);
        }
    }

    template <typename data_t>
    bool GradientDescent<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherGD = downcast_safe<GradientDescent<data_t>>(&other);
        if (!otherGD)
            return false;

        if ((_lineSearchMethod and not otherGD->_lineSearchMethod)
            or (not _lineSearchMethod and otherGD->_lineSearchMethod)) {
            return false;
        } else if (not _lineSearchMethod and not otherGD->_lineSearchMethod) {
            return true;
        } else {
            return _lineSearchMethod->isEqual(*(otherGD->_lineSearchMethod));
        }
    }

    // ------------------------------------------
    // explicit template instantiation
    template class GradientDescent<float>;
    template class GradientDescent<double>;
} // namespace elsa
