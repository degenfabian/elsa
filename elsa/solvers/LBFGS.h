#pragma once

#include <optional>

#include "Solver.h"
#include "Functional.h"
#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief L-BFGS solver
     *
     * Limited Memory Broyden–Fletcher–Goldfarb–Shanno (L-BFGS) is a Limited-Memory quasi-Newton
     * iterative optimization algorithm for non-linear problems. L-BFGS minimizes an objective
     * function @f$f@f$ by first constructing an approximation of it as:
     * @f[
     * m(d) = f(x_i) + \nablaf_i^T d + frac{1}{2}d^T H^{-1}_i d
     * @f]
     *
     * where @f$f: \mathbb{R}^n \to \mathbb{R}@f$ is differentiable,
     * @f$\nabla f_i: \mathbb{R}^n is the gradient of f at x_i@f$,
     * @f$ d: \mathbb{R}^n is a search direction @f$,
     * @f$ H^{-1}_i: \mathbb{R}^{n \times n} is an approximation of the Hessian \nabla^2 f(x_i) @f$
     *
     * The search direction @f$d@f$ that minimizes the approximation @f$m(d)@f$ is
     * analytically found to be @f$d_i = H_i \nabla f_i@f$.
     *
     * Unlike BFGS, L-BFGS does not explicitly calculate the inverse Hessian @f$H@f$,
     * instead it approximates its effects by applying the following formula to find the
     * search direction:
     *
     * @f[
     * H_{i} = (V_{i-1}^T ... V_{i-m}^T) H_0 (V_{i-m} ... V_{i-1})
     *       + \rho_{i-m} (V_{i-1}^T ... V_{i - m + 1}^T) s_{i-m} s_{i-m}^T
     *       (V_{i-m+1} ... V_{i-1})
     *       + \rho_{i-m+1} (V_{i-1}^T ... V_{i - m + 2}^T) s_{i-m+1} s_{i-m+1}^T
     *       (V_{i-m+2} ... V_{i-1})
     *       + ...
     *       + \rho_{i-1} s_{i-1} s_{i-1}^T
     * @f]
     *
     * where:
     * @f$ H^_i: \mathbb{R}^{n \times n} is an approximation of the inverse Hessian
     * (\nabla^2f(x_i))^{-1}@f$
     * @f$V_i: \mathbb{R}^{n \times \n} I - \rho_i y_i s_i^T@f$,
     * @f$ \rho_i: frac{1}{y_i^T s_i}@f$,
     * @f$ s_i: \mathbb{R}^{n} = x_{i+1} - x_i  @f$, and
     * @f$ y_i: \mathbb{R}^{n} = \nabla f_{i+1} - \nabla f_i  @f$
     *
     * References:
     * - See Wright and Nocedal, ‘Numerical Optimization’, 2nd Edition, 2006, pp. 176-179.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class LBFGS : public Solver<data_t>
    {
    public:
        /// Scalar alias
        using Scalar = typename Solver<data_t>::Scalar;

        LBFGS(const Functional<data_t>& problem, const LineSearchMethod<data_t>& line_search_method,
              const index_t& memory = 10, const data_t& tol = 1e-4);

        /// make copy constructor deletion explicit
        LBFGS(const LBFGS<data_t>&) = delete;

        /// default destructor
        ~LBFGS() override = default;

        DataContainer<data_t>
            solve(index_t iterations,
                  std::optional<DataContainer<data_t>> x0 = std::nullopt) override;

    private:
        /// the differentiable optimizaion problem
        std::unique_ptr<Functional<data_t>> _problem;

        /// the line search method
        // TODO: maybe change this to be only strong wolfe?
        std::unique_ptr<LineSearchMethod<data_t>> _ls;

        index_t _m;

        /// the step size
        data_t _tol;

        /// implement the polymorphic clone operation
        LBFGS<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Solver<data_t>& other) const override;
    };
} // namespace elsa
