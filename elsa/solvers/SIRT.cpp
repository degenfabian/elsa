#include "SIRT.h"
#include "Scaling.h"
#include "elsaDefines.h"

namespace elsa
{
    template <typename data_t>
    SIRT<data_t>::SIRT(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       SelfType_t<data_t> stepSize)
        : LandweberIteration<data_t>(A, b, stepSize)
    {
        this->name_ = "SIRT";
    }

    template <typename data_t>
    SIRT<data_t>::SIRT(const LinearOperator<data_t>& A, const DataContainer<data_t>& b)
        : LandweberIteration<data_t>(A, b)
    {
        this->name_ = "SIRT";
    }

    template <class data_t>
    std::unique_ptr<LinearOperator<data_t>>
        SIRT<data_t>::setupOperators(const LinearOperator<data_t>& A) const
    {
        const auto& domain = A.getDomainDescriptor();
        const auto& range = A.getRangeDescriptor();

        auto rowsum = A.apply(domain.template element<data_t>().one());

        // Prevent division by zero (and hence NaNs), by slightly lifting everything up a touch
        rowsum += data_t{1e-10f};

        Scaling<data_t> M(data_t{1.} / rowsum);

        auto colsum = A.applyAdjoint(range.template element<data_t>().one());
        Scaling<data_t> T(data_t{1.} / colsum);

        return (T * adjoint(A) * M).clone();
    }

    template <typename data_t>
    bool SIRT<data_t>::isEqual(const Solver<data_t>& other) const
    {
        if (!LandweberIteration<data_t>::isEqual(other))
            return false;

        auto otherSolver = downcast_safe<SIRT<data_t>>(&other);
        return static_cast<bool>(otherSolver);
    }

    template <typename data_t>
    SIRT<data_t>* SIRT<data_t>::cloneImpl() const
    {
        if (this->stepSize_.isInitialized()) {
            return new SIRT(*this->A_, this->b_, *this->stepSize_);
        } else {
            return new SIRT(*this->A_, this->b_);
        }
    }

    // ------------------------------------------
    // explicit template instantiation
    template class SIRT<float>;
    template class SIRT<double>;
} // namespace elsa
