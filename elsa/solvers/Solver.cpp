#include "Solver.h"
#include "DataContainer.h"
#include "spdlog/stopwatch.h"
#include <chrono>

namespace elsa
{
    template <class data_t>
    DataContainer<data_t> Solver<data_t>::solve(index_t iterations,
                                                std::optional<DataContainer<data_t>> x0)
    {
        auto x = setup(x0);
        x = run(iterations, x, true);
        return x;
    }

    template <class data_t>
    DataContainer<data_t> Solver<data_t>::setup(std::optional<DataContainer<data_t>>)
    {
        throw Error("Solver: setup is not implemented for this solver");
    }

    template <class data_t>
    DataContainer<data_t> Solver<data_t>::step(DataContainer<data_t>)
    {
        throw Error("Solver: step is not implemented for this solver");
    }

    template <class data_t>
    DataContainer<data_t> Solver<data_t>::run(index_t iterations, const DataContainer<data_t>& x0,
                                              bool show)
    {
        auto x = materialize(x0);

        if (!configured_) {
            x = setup(x);
        }

        if (show) {
            printHeader();
        }

        spdlog::stopwatch aggregate_time;

        auto enditer = std::min(curiter_ + iterations, maxiters_);
        while (curiter_ < enditer && !shouldStop()) {
            spdlog::stopwatch steptime;
            x = step(std::move(x));
            auto stepelapsed = steptime.elapsed();
            auto totalelapsed = aggregate_time.elapsed();

            callback_(x);

            if (show && curiter_ % printEvery_ == 0) {
                printStep(x, curiter_, stepelapsed, totalelapsed);
            }
            ++curiter_;
        }

        return x;
    }

    template <class data_t>
    bool Solver<data_t>::shouldStop() const
    {
        return false;
    }

    template <class data_t>
    std::string Solver<data_t>::formatHeader() const
    {
        return "";
    }

    template <class data_t>
    void Solver<data_t>::printHeader() const
    {
        auto str = formatHeader();
        Logger::get(name_)->info("{:^5} | {:^8} | {:^8} | {} |", "Iters", "time (s)", "elapsed",
                                 str);
    }

    template <class data_t>
    std::string Solver<data_t>::formatStep(const DataContainer<data_t>& /* x */) const
    {
        return "";
    }

    template <class data_t>
    void Solver<data_t>::printStep(const DataContainer<data_t>& x, index_t curiter,
                                   std::chrono::duration<double> steptime,
                                   std::chrono::duration<double> elapsed) const
    {
        auto str = formatStep(x);
        Logger::get(name_)->info("{:>5} | {:>8.3} | {:>8.3} | {} |", curiter, steptime.count(),
                                 elapsed.count(), str);
    }

    template <class data_t>
    void Solver<data_t>::setMaxiters(index_t maxiters)
    {
        maxiters_ = maxiters;
    }

    template <class data_t>
    void Solver<data_t>::setCallback(
        const std::function<void(const DataContainer<data_t>&)>& callback)
    {
        callback_ = callback;
    }

    template <class data_t>
    void Solver<data_t>::printEvery(index_t printevery)
    {
        printEvery_ = printevery;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class Solver<float>;
    template class Solver<double>;
    template class Solver<complex<float>>;
    template class Solver<complex<double>>;
} // namespace elsa
