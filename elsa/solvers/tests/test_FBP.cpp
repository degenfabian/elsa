#include "DataContainer.h"
#include "doctest/doctest.h"

#include "FBP.h"
#include "Logger.h"
#include "VolumeDescriptor.h"
#include "SiddonsMethod.h"
#include "CircleTrajectoryGenerator.h"
#include "Phantoms.h"
#include "TypeCasts.hpp"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("solvers");

TEST_CASE("SiddonsMethod Shepp-Logan Phantom Reconstruction")
{
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a Phantom reconstruction problem")
    {

        IndexVector_t size({{50, 50}});
        auto phantom = phantoms::modifiedSheppLogan(size);
        auto& volumeDescriptor = phantom.getDataDescriptor();

        // generate circular trajectory
        index_t numAngles{200}, arc{360};
        const auto distance = static_cast<real_t>(size(0));
        auto sinoDescriptor = CircleTrajectoryGenerator::createTrajectory(
            numAngles, phantom.getDataDescriptor(), arc, distance * 10000.0f, distance);

        // dynamic_cast to VolumeDescriptor is legal and will not throw, as Phantoms returns a
        // VolumeDescriptor
        SiddonsMethod projector(dynamic_cast<const VolumeDescriptor&>(volumeDescriptor),
                                *sinoDescriptor);

        // simulate the sinogram
        auto sinogram = projector.apply(phantom);

        auto ramlak = makeRamLakFilter(sinogram.getDataDescriptor());

        WHEN("setting up a FBP solver")
        {
            FBP<float> fbp{projector, std::move(ramlak)};

            THEN("applying it")
            {
                auto reconstruction = fbp.apply(sinogram);
                DataContainer resultsDifference = elsa::maximum(reconstruction - phantom, 0);
                REQUIRE_LE(
                    resultsDifference.l2Norm(),
                    epsilon
                        * volumeDescriptor.getNumberOfCoefficients()); // Should roughly look equal
            }
        }
    }
}

TEST_SUITE_END();