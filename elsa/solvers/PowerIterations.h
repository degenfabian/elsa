#pragma once

#include "DataContainer.h"
#include "LinearOperator.h"

namespace elsa
{
    /// @brief The power iterations approximate the largest eigenvalue of the given operator.
    /// The operator must be symmetric, else an exception is thrown.
    ///
    /// @param op the operator to approximate the operator norm
    /// @param maxiters the maximum number of iterations to run
    /// @param rtol relative tolerance parameter
    /// @param atol absolute tolerance parameter
    template <class data_t>
    data_t powerIterations(const LinearOperator<data_t>& op, index_t maxiters = 100,
                           real_t rtol = 1e-05, real_t atol = 1e-08);
} // namespace elsa
