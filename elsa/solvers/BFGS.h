#pragma once

#include <optional>

#include "Solver.h"
#include "Functional.h"
#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief BFGS solver
     *
     * Broyden–Fletcher–Goldfarb–Shanno (BFGS) is a quasi-Newton iterative optimization algorithm
     * for non-linear problems.
     * BFGS minimizes an objective function @f$f@f$ by first constructing an
     * approximation of it as:
     * @f[
     * m(d) = f(x_i) + \nablaf_i^T d + frac{1}{2}d^T H^{-1}_i d
     * @f]
     *
     * where @f$f: \mathbb{R}^n \to \mathbb{R}@f$ is differentiable,
     * @f$\nabla f_i: \mathbb{R}^n is the gradient of f at x_i@f$,
     * @f$ d: \mathbb{R}^n is a search direction @f$,
     * @f$ H^{-1}_i: \mathbb{R}^{n \times n} is an approximation of the Hessian \nabla^2 f(x_i) @f$
     *
     * The search direction @f$d@f$ that minimizes the approximation @f$m(d)@f$ is
     * analytically found to be @f$d_i = H_i \nabla f_i@f$.
     *
     * @f$H_i@f$ is calculated by the following formula:
     *
     * @f[
     * H_{i+1} = (I - \rho_i s_i y_i^T) H_i (I - \rho y_i s_i^T) + \rho s_i s_i^T
     * @f]
     *
     * where:
     * @f$ H^_i: \mathbb{R}^{n \times n} is an approximation of the inverse Hessian
     * (\nabla^2f(x_i))^{-1}@f$
     * @f$I: \mathbb{R}^{n \times \n} is identity matrix@f$,
     * @f$ \rho_i: frac{1}{y_i^T s_i}@f$,
     * @f$ s_i: \mathbb{R}^{n} = x_{i+1} - x_i  @f$, and
     * @f$ y_i: \mathbb{R}^{n} = \nabla f_{i+1} - \nabla f_i  @f$
     *
     * References:
     * - See Wright and Nocedal, ‘Numerical Optimization’, 2nd Edition, 2006, pp. 136-145.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class BFGS : public Solver<data_t>
    {
    public:
        /// Scalar alias
        using Scalar = typename Solver<data_t>::Scalar;

        BFGS(const Functional<data_t>& problem, const LineSearchMethod<data_t>& line_search_method,
             const data_t& tol = 1e-4);

        /// make copy constructor deletion explicit
        BFGS(const BFGS<data_t>&) = delete;

        /// default destructor
        ~BFGS() override = default;

        DataContainer<data_t>
            solve(index_t iterations,
                  std::optional<DataContainer<data_t>> x0 = std::nullopt) override;

    private:
        /// the differentiable optimizaion problem
        std::unique_ptr<Functional<data_t>> _problem;

        /// the line search method
        // TODO: maybe change this to be only strong wolfe?
        std::unique_ptr<LineSearchMethod<data_t>> _ls;

        /// the step size
        data_t _tol;

        /// implement the polymorphic clone operation
        BFGS<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Solver<data_t>& other) const override;
    };
} // namespace elsa
