#pragma once

#include "IdenticalBlocksDescriptor.h"
#include "elsaDefines.h"

namespace elsa
{

    /**
     * @brief A descriptor representing e.g. a volume descriptor of spherical harmonics
     * coefficients. These get stored as identical blocks, alongside with a symmetry hint
     * (oftentimes we only store even harmonics) and the maximum degree of the spherical harmonics.
     * @author Cederik Höfs
     */
    class SphericalCoefficientsDescriptor : public IdenticalBlocksDescriptor
    {
        using Symmetry = axdt::Symmetry;

    public:
        /**
         * @brief Constructs a SphericalCoefficientsDescriptor object.
         *
         * @param blockDescriptor The data descriptor for the block.
         * @param symmetry The symmetry of the spherical coefficients.
         * @param degree The degree of the spherical coefficients.
         */
        SphericalCoefficientsDescriptor(const DataDescriptor& blockDescriptor,
                                        const Symmetry symmetry, const index_t degree);

        /**
         * @brief Deleted copy constructor.
         * We use clone() instead.
         */
        SphericalCoefficientsDescriptor(const SphericalCoefficientsDescriptor&) = delete;

        /**
         * @brief Default destructor.
         */
        ~SphericalCoefficientsDescriptor() override = default;

        /**
         * @brief Calculates the total number of coefficients for a given symmetry and degree.
         *
         * @param symmetry The symmetry of the spherical coefficients.
         * @param degree The degree of the spherical coefficients.
         * @return The total number of coefficients.
         */
        static inline constexpr index_t coefficientCount(const Symmetry symmetry,
                                                         const index_t degree)
        {
            if (symmetry == Symmetry::even)
                return (degree + 1) * (degree / 2 + 1);
            else
                return (degree + 1) * (degree + 1);
        }

        const index_t degree;    /**< The degree of the spherical coefficients. */
        const Symmetry symmetry; /**< The symmetry of the spherical coefficients. */

    protected:
        /**
         * @brief Creates a copy of the SphericalCoefficientsDescriptor object.
         *
         * @return A pointer to the cloned SphericalCoefficientsDescriptor object.
         */
        SphericalCoefficientsDescriptor* cloneImpl() const override;

        /**
         * @brief Checks if the SphericalCoefficientsDescriptor object is equal to another
         * DataDescriptor object.
         *
         * @param other The other DataDescriptor object to compare with.
         * @return True if the objects are equal, false otherwise.
         */
        bool isEqual(const DataDescriptor& other) const override;
    };
} // namespace elsa
