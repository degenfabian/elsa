#include "NdView.h"

namespace elsa::detail
{
    bool are_strides_compatible(const IndexVector_t& shape1, const IndexVector_t& strides1,
                                const IndexVector_t& shape2, const IndexVector_t& strides2)
    {
        index_t dim_count = shape1.size();
        if (dim_count != shape2.size()) {
            throw NdViewDimError();
        }
        size_t element_count = 1;
        bool strides_compatible = true;
        for (index_t i = 0; i < dim_count; i++) {
            if (shape1(i) != shape2(i)) {
                throw NdViewDimError();
            }
            strides_compatible &= strides1(i) == strides2(i);
            element_count *= shape1(i);
        }
        return strides_compatible;
    }
} // namespace elsa::detail
