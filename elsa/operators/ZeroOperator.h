#pragma once

#include "LinearOperator.h"

namespace elsa
{
    /**
     * @brief Operator that returns empty container filled with 0.
     *
     * @tparam data_t data type for the domain and range of the operator, defaulting to real_t
     *
     */
    template <typename data_t = real_t>
    class ZeroOperator : public LinearOperator<data_t>
    {
    public:
        explicit ZeroOperator(const DataDescriptor& domainDescriptor,
                              const DataDescriptor& rangeDescriptor);

        ~ZeroOperator() override = default;

    protected:
        /// default copy constructor, hidden from non-derived classes to prevent potential slicing
        ZeroOperator(const ZeroOperator<data_t>&) = default;

        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

        /// implement the polymorphic clone operation
        ZeroOperator<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LinearOperator<data_t>& other) const override;
    };
} // namespace elsa
