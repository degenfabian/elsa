#include "SymmetrizedDerivative.h"
#include "Timer.h"
#include "Identity.h"
#include "FiniteDifferences.h"
#include "BlockLinearOperator.h"
#include "DataContainer.h"

namespace elsa
{

    using namespace std;

    template <typename data_t>
    SymmetrizedDerivative<data_t>::SymmetrizedDerivative(const DataDescriptor& domainDescriptor)
        : LinearOperator<data_t>(createBase(domainDescriptor))
    {
        precomputeHelpers();
    }

    using namespace std;

    template <typename data_t>
    void SymmetrizedDerivative<data_t>::applyImpl(const DataContainer<data_t>& x,
                                                  DataContainer<data_t>& Ax) const
    {
        Timer timeguard("SymmetrizedDerivative", "apply");

        // D_1 x_1
        Ax.getBlock(0) = forwardX_->apply(x.getBlock(0));

        // D_2 x_2
        Ax.getBlock(1) = forwardY_->apply(x.getBlock(1));

        // 0.5 (D_2 x_1 + D_1 x_2)
        Ax.getBlock(2) = lincomb(data_t(0.5), forwardY_->apply(x.getBlock(0)), data_t(0.5),
                                 forwardX_->apply(x.getBlock(1)));
    }

    template <typename data_t>
    void SymmetrizedDerivative<data_t>::applyAdjointImpl(const DataContainer<data_t>& y,
                                                         DataContainer<data_t>& Aty) const
    {
        Timer timeguard("SymmetrizedDerivative", "applyAdjoint");

        //-D_1^{-} y_1 - D_2^{-} y_3
        Aty.getBlock(0) = lincomb(data_t(-1), backwardX_->apply(y.getBlock(0)), data_t(-1),
                                  backwardY_->apply(y.getBlock(2)));

        //-D_1^{-} y_3 - D_2^{-} y_2
        Aty.getBlock(1) = lincomb(data_t(-1), backwardX_->apply(y.getBlock(2)), data_t(-1),
                                  backwardY_->apply(y.getBlock(1)));
    }

    template <typename data_t>
    void SymmetrizedDerivative<data_t>::precomputeHelpers()
    {

        auto& domainBlocked = downcast_safe<BlockDescriptor>(this->getDomainDescriptor());

        BooleanVector_t bv1(2);
        bv1 << true, false;
        BooleanVector_t bv2(2);
        bv2 << false, true;

        forwardX_ =
            make_unique<FiniteDifferences<data_t>>(domainBlocked.getDescriptorOfBlock(0), bv1);
        forwardY_ =
            make_unique<FiniteDifferences<data_t>>(domainBlocked.getDescriptorOfBlock(0), bv2);
        backwardX_ =
            make_unique<FiniteDifferences<data_t>>(domainBlocked.getDescriptorOfBlock(0), bv1,
                                                   FiniteDifferences<data_t>::DiffType::BACKWARD);
        backwardY_ =
            make_unique<FiniteDifferences<data_t>>(domainBlocked.getDescriptorOfBlock(0), bv2,
                                                   FiniteDifferences<data_t>::DiffType::BACKWARD);
    }

    template <typename data_t>
    LinearOperator<data_t>
        SymmetrizedDerivative<data_t>::createBase(const DataDescriptor& domainDescriptor)
    {

        if (!is<IdenticalBlocksDescriptor>(domainDescriptor))
            throw LogicError("SymmetrizedDerivative: cannot construct. Domain should be "
                             "IdenticalBlocksDescriptor");

        auto& domainBlocked = downcast_safe<BlockDescriptor>(domainDescriptor);
        if (domainBlocked.getNumberOfBlocks() != 2)
            throw LogicError(
                "SymmetrizedDerivative: cannot construct. Domain should have 2 blocks");

        if (domainBlocked.getDescriptorOfBlock(0).getNumberOfDimensions() != 2)
            throw LogicError("SymmetrizedDerivative: cannot construct. Domain blocks should be "
                             "identical and 2-dimensional");

        return LinearOperator<data_t>(
            domainBlocked, IdenticalBlocksDescriptor{3, domainBlocked.getDescriptorOfBlock(0)});
    }

    template <typename data_t>
    SymmetrizedDerivative<data_t>* SymmetrizedDerivative<data_t>::cloneImpl() const
    {
        return new SymmetrizedDerivative(this->getDomainDescriptor());
    }

    template <typename data_t>
    bool SymmetrizedDerivative<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        auto otherSD = downcast_safe<SymmetrizedDerivative>(&other);
        if (!otherSD)
            return false;

        if (otherSD->getDomainDescriptor() != this->getDomainDescriptor()
            || otherSD->getRangeDescriptor() != this->getRangeDescriptor())
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class SymmetrizedDerivative<float>;
    template class SymmetrizedDerivative<double>;

} // namespace elsa