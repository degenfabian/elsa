#pragma once

#include "BlockLinearOperator.h"
#include "SphericalCoefficientsDescriptor.h"
#include "XGIDetectorDescriptor.h"
#include "LinearOperator.h"
#include "elsaDefines.h"

namespace elsa
{
    /**
     * @brief The AXDT operator combines an arbitrary projector with
     * a column block linear operator, which represents the forward model
     * of the anisotropic dark-field signal
     * For details check https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.117.158101
     * @author Matthias Wieczorek (wieczore@cs.tum.edu), original implementation
     * @author Nikola Dinev (nikola.dinev@tum.de), port to elsa
     * @author Shen Hu (shen.hu@tum.de), rewrite, integrate, port to elsa
     * @author Cederik Höfs (cederik.hoefs@tum.de) refactor
     *
     * @tparam real_t real type
     *
     */
    template <typename data_t>
    class AXDTOperator : public LinearOperator<data_t>
    {
    public:
        using OperatorList = typename BlockLinearOperator<data_t>::OperatorList;
        using Symmetry = axdt::Symmetry;

        /**
         * @brief Construct an AXDTOperator
         *
         * @param[in] domainDescriptor descriptor of the domain of the operator (the
         * reconstructed volume)
         * @param[in] rangeDescriptor descriptor of the range of the operator (the XGI Detector
         * descriptor)
         * @param[in] projector the projector representing the line integral
         * @param[in] weights optional pre-computed weighting function for the spherical
         * coefficients, shape is (rangeDescriptor) coefficientCount(symmetry, maxL)
         *
         */
        AXDTOperator(const SphericalCoefficientsDescriptor& domainDescriptor,
                     const XGIDetectorDescriptor& rangeDescriptor,
                     const LinearOperator<data_t>& projector,
                     std::optional<DataContainer<data_t>> weights = std::nullopt);

        ~AXDTOperator() override = default;

    protected:
        /// protected copy constructor; used for cloning
        AXDTOperator(const AXDTOperator& other);

        /// implement the polymorphic clone operation
        AXDTOperator<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LinearOperator<data_t>& other) const override;

        /// apply the AXDT operator
        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        /// apply the adjoint of the AXDT operator
        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

    private:
        /// Ptr to a BlockLinearOperator, though saved as a ptr to LinearOperator
        std::unique_ptr<BlockLinearOperator<data_t>> bl_op;
    };

} // namespace elsa
