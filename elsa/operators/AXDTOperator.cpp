#include "AXDTOperator.h"
#include "SphericalCoefficientsDescriptor.h"
#include "WeightingFunction.h"
#include "BlockLinearOperator.h"
#include "DataContainer.h"
#include "Scaling.h"
#include "Timer.h"
#include "TypeCasts.hpp"
#include "elsaDefines.h"

namespace elsa
{

    template <typename data_t>
    AXDTOperator<data_t>::AXDTOperator(const SphericalCoefficientsDescriptor& domainDescriptor,
                                       const XGIDetectorDescriptor& rangeDescriptor,
                                       const LinearOperator<data_t>& projector,
                                       std::optional<DataContainer<data_t>> weights)
        : LinearOperator<data_t>(domainDescriptor, rangeDescriptor)
    {

        auto W = [&]() {
            if (weights)
                return *weights;
            else
                return axdt::exactWeightingFunction<data_t>(
                    rangeDescriptor, domainDescriptor.symmetry, domainDescriptor.degree);
        }();

        // Number of spherical harmonics
        assert(W.getNumberOfBlocks() == domainDescriptor.getNumberOfBlocks());

        OperatorList ops;

        // create composite operators of projector and scalings
        for (index_t i = 0; i < W.getNumberOfBlocks(); ++i) {
            // create a scaling operator
            Scaling<data_t> W_i{materialize(W.getBlock(i))};

            ops.emplace_back((W_i * projector).clone());
        }

        bl_op = std::make_unique<BlockLinearOperator<data_t>>(
            *this->_domainDescriptor, rangeDescriptor, ops,
            BlockLinearOperator<data_t>::BlockType::COL);
    }

    template <typename data_t>
    AXDTOperator<data_t>::AXDTOperator(const AXDTOperator& other)
        : LinearOperator<data_t>(*other._domainDescriptor, *other._rangeDescriptor)
    {
        bl_op = downcast<BlockLinearOperator<data_t>>(other.bl_op->clone());
    }

    template <typename data_t>
    AXDTOperator<data_t>* AXDTOperator<data_t>::cloneImpl() const
    {
        return new AXDTOperator<data_t>(*this);
    }

    template <typename data_t>
    bool AXDTOperator<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        // static_cast as type checked in base comparison
        const auto& otherOp = downcast<const AXDTOperator<data_t>>(other);

        return *bl_op == *(otherOp.bl_op);
    }

    template <typename data_t>
    void AXDTOperator<data_t>::applyImpl(const DataContainer<data_t>& x,
                                         DataContainer<data_t>& Ax) const
    {
        Timer timeguard("AXDTOperator", "apply");

        bl_op->apply(x, Ax);
    }

    template <typename data_t>
    void AXDTOperator<data_t>::applyAdjointImpl(const DataContainer<data_t>& y,
                                                DataContainer<data_t>& Aty) const
    {
        Timer timeguard("AXDTOperator", "applyAdjoint");

        bl_op->applyAdjoint(y, Aty);
        //        Logger::get("AXDTOperator")->info("ApplyAdjoint result {}", Aty.sum());
    }

    template class AXDTOperator<float>;
    template class AXDTOperator<double>;

} // namespace elsa
