/**
 * @file test_AXDTOperator.cpp
 *
 * @brief Tests for the ADXTOperator
 *
 * @author Shen Hu - main code
 */

#include "SphericalCoefficientsDescriptor.h"
#include "doctest/doctest.h"
#include "AXDTOperator.h"
#include "WeightingFunction.h"
#include "StrongTypes.h"
#include "DataContainer.h"
#include "Identity.h"
#include "Scaling.h"
#include "Logger.h"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;
using namespace elsa::axdt;

TEST_SUITE_BEGIN("core");

TEST_CASE_TEMPLATE("AXDTOperator: Testing apply & applyAdjoint", data_t, float, double)
{
    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);
    srand((unsigned int) 666);

    const index_t domainSize{2};
    IndexVector_t domain{3};
    domain << domainSize, domainSize, domainSize;
    VolumeDescriptor volDesc{domain};

    const index_t rangeSize{domainSize};
    const index_t numOfImgs{domainSize};
    IndexVector_t range{3};
    range << rangeSize, rangeSize, numOfImgs;
    RealVector_t rangeSpacing{3};
    rangeSpacing << 1, 1, 1;

    geometry::SourceToCenterOfRotation s2c{100};
    geometry::CenterOfRotationToDetector c2d{50};

    // arbitrary geometry
    std::vector<Geometry> geos;
    for (index_t i = 0; i < numOfImgs; ++i) {
        geos.emplace_back(s2c, c2d, geometry::VolumeData3D{geometry::Size3D{domain}},
                          geometry::SinogramData3D{geometry::Size3D{range}},
                          geometry::Gamma{static_cast<real_t>(i)});
    }

    axdt::DirVecList<data_t> samplingPattern;
    samplingPattern.emplace_back(0.50748, -0.3062, 0.80543);
    samplingPattern.emplace_back(-0.3062, 0.80543, 0.50748);
    samplingPattern.emplace_back(-0.50748, 0.3062, 0.80543);
    samplingPattern.emplace_back(0.80543, 0.50748, -0.3062);
    samplingPattern.emplace_back(0.3062, 0.80543, -0.50748);
    samplingPattern.emplace_back(0.80543, -0.50748, 0.3062);
    samplingPattern.emplace_back(0.3062, -0.80543, 0.50748);
    samplingPattern.emplace_back(-0.80543, -0.50748, -0.3062);
    samplingPattern.emplace_back(-0.3062, -0.80543, -0.50748);
    samplingPattern.emplace_back(-0.80543, 0.50748, 0.3062);
    samplingPattern.emplace_back(0.50748, 0.3062, -0.80543);
    samplingPattern.emplace_back(-0.50748, -0.3062, -0.80543);
    samplingPattern.emplace_back(0.62636, -0.24353, -0.74052);
    samplingPattern.emplace_back(-0.24353, -0.74052, 0.62636);
    samplingPattern.emplace_back(-0.62636, 0.24353, -0.74052);
    samplingPattern.emplace_back(-0.74052, 0.62636, -0.24353);
    samplingPattern.emplace_back(0.24353, -0.74052, -0.62636);
    samplingPattern.emplace_back(-0.74052, -0.62636, 0.24353);
    samplingPattern.emplace_back(0.24353, 0.74052, 0.62636);
    samplingPattern.emplace_back(0.74052, -0.62636, -0.24353);
    samplingPattern.emplace_back(-0.24353, 0.74052, -0.62636);
    samplingPattern.emplace_back(0.74052, 0.62636, 0.24353);
    samplingPattern.emplace_back(0.62636, 0.24353, 0.74052);
    samplingPattern.emplace_back(-0.62636, -0.24353, 0.74052);
    samplingPattern.emplace_back(-0.28625, 0.95712, -0.044524);
    samplingPattern.emplace_back(0.95712, -0.044524, -0.28625);
    samplingPattern.emplace_back(0.28625, -0.95712, -0.044524);
    samplingPattern.emplace_back(-0.044524, -0.28625, 0.95712);
    samplingPattern.emplace_back(-0.95712, -0.044524, 0.28625);
    samplingPattern.emplace_back(-0.044524, 0.28625, -0.95712);
    samplingPattern.emplace_back(-0.95712, 0.044524, -0.28625);
    samplingPattern.emplace_back(0.044524, 0.28625, 0.95712);
    samplingPattern.emplace_back(0.95712, 0.044524, 0.28625);
    samplingPattern.emplace_back(0.044524, -0.28625, -0.95712);
    samplingPattern.emplace_back(-0.28625, -0.95712, 0.044524);
    samplingPattern.emplace_back(0.28625, 0.95712, 0.044524);

    GIVEN("an AXDTOperator with an identity operator as the projector")
    {
        XGIDetectorDescriptor xgiDesc{range, rangeSpacing, geos,
                                      XGIDetectorDescriptor::DirVec(1, 0, 0), true};

        Identity<data_t> proj(volDesc); // pretend to be a legit projector... (restricted
                                        // domain/range dimension sizes)

        const auto symmetry = axdt::Symmetry::even;
        const auto maxL = 4;

        SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};

        AXDTOperator<data_t> axdtOp{coeffDesc, xgiDesc, proj};

        WHEN("apply the transform")
        {
            SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};
            DataContainer<data_t> input(
                coeffDesc,
                Eigen::Matrix<data_t, 15 * domainSize * domainSize * domainSize, 1>::Ones());
            DataContainer<data_t> output(xgiDesc);
            axdtOp.apply(input, output);

            auto numericalWeights =
                axdt::numericalWeightingFunction<data_t>(xgiDesc, symmetry, maxL, samplingPattern);
            auto exactWeights = axdt::exactWeightingFunction<data_t>(xgiDesc, symmetry, maxL);

            THEN("results should be close to ground truth")
            {
                Vector_t<double> groundTruth(rangeSize * rangeSize * numOfImgs);
                groundTruth << 1.10996, 1.10996, 1.10996, 1.10996, 0.597108, 0.597108, 0.597108,
                    0.597108;

                for (index_t i = 0; i < xgiDesc.getNumberOfCoefficients(); ++i) {
                    REQUIRE(output[i] == Approx(groundTruth[i]).epsilon(1e-3));
                }
            }
        }

        WHEN("apply the adjoint transform")
        {
            SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};
            DataContainer<data_t> input{coeffDesc};
            DataContainer<data_t> output(
                xgiDesc, Eigen::Matrix<data_t, rangeSize * rangeSize * numOfImgs, 1>::Ones());
            axdtOp.applyAdjoint(output, input);

            THEN("results should be close to ground truth")
            {
                Vector_t<double> groundTruth(domainSize * domainSize * domainSize * 15);
                groundTruth << 0.945309, 0.945309, 0.945309, 0.945309, 0.945309, 0.945309, 0.945309,
                    0.945309, 9.31323e-10, 9.31323e-10, 9.31323e-10, 9.31323e-10, 7.45058e-09,
                    7.45058e-09, 7.45058e-09, 7.45058e-09, -3.25963e-08, -3.25963e-08, -3.25963e-08,
                    -3.25963e-08, 0, 0, 0, 0, -0.603937, -0.603937, -0.603937, -0.603937, 0.518595,
                    0.518595, 0.518595, 0.518595, -7.45058e-09, -7.45058e-09, -7.45058e-09,
                    -7.45058e-09, -0.832273, -0.832273, -0.832273, -0.832273, 0.784536, 0.784536,
                    0.784536, 0.784536, 0.136442, 0.136442, 0.136442, 0.136442, -1.86265e-08,
                    -1.86265e-08, -1.86265e-08, -1.86265e-08, 1.11759e-08, 1.11759e-08, 1.11759e-08,
                    1.11759e-08, 6.33299e-08, 6.33299e-08, 6.33299e-08, 6.33299e-08, 7.45058e-09,
                    7.45058e-09, 7.45058e-09, 7.45058e-09, -2.6077e-08, -2.6077e-08, -2.6077e-08,
                    -2.6077e-08, -4.47035e-08, -4.47035e-08, -4.47035e-08, -4.47035e-08,
                    4.47035e-08, 4.47035e-08, 4.47035e-08, 4.47035e-08, 4.47035e-08, 4.47035e-08,
                    4.47035e-08, 4.47035e-08, 0.135046, 0.135046, 0.135046, 0.135046, -0.109205,
                    -0.109205, -0.109205, -0.109205, 1.49012e-08, 1.49012e-08, 1.49012e-08,
                    1.49012e-08, -0.141402, -0.141402, -0.141402, -0.141402, -0.150987, -0.150987,
                    -0.150987, -0.150987, 0.0674836, 0.0674836, 0.0674836, 0.0674836, 1.49012e-08,
                    1.49012e-08, 1.49012e-08, 1.49012e-08, 0.0534442, 0.0534442, 0.0534442,
                    0.0534442, -4.67896e-06, -4.67896e-06, -4.67896e-06, -4.67896e-06, -0.0412863,
                    -0.0412863, -0.0412863, -0.0412863;

                for (index_t i = 0; i < input.getDataDescriptor().getNumberOfCoefficients(); ++i) {
                    REQUIRE(input[i] == Approx(groundTruth[i]).epsilon(1e-3));
                }
            }
        }
    }
    GIVEN("an AXDTOperator with an anisotropic scaling operator as the projector")
    {
        XGIDetectorDescriptor xgiDesc{range, rangeSpacing, geos,
                                      XGIDetectorDescriptor::DirVec(1, 0, 0), true};

        Eigen::Matrix<data_t, domainSize * domainSize * domainSize, 1> rawScaleValues;
        rawScaleValues << 2, 3, 5, 7, 11, 13, 17, 19;
        DataContainer<data_t> scaleValues(volDesc, rawScaleValues);
        Scaling proj(scaleValues); // pretend to be a legit projector... (restricted
                                   // domain/range dimension sizes)

        const auto symmetry = Symmetry::even;
        const auto maxL = 4;

        SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};

        AXDTOperator<data_t> axdtOp{coeffDesc, xgiDesc, proj};

        WHEN("apply the transform")
        {
            SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};
            DataContainer<data_t> input(
                coeffDesc,
                Eigen::Matrix<data_t, 15 * domainSize * domainSize * domainSize, 1>::Ones());
            DataContainer<data_t> output(xgiDesc);
            axdtOp.apply(input, output);

            THEN("results should be close to ground truth")
            {
                Vector_t<double> groundTruth(rangeSize * rangeSize * numOfImgs);
                groundTruth << 2.21993, 3.32989, 5.54981, 7.76974, 6.56819, 7.7624, 10.1508, 11.345;

                for (index_t i = 0; i < xgiDesc.getNumberOfCoefficients(); ++i) {
                    REQUIRE(output[i] == Approx(groundTruth[i]).epsilon(1e-3));
                }
            }
        }

        WHEN("apply the adjoint transform")
        {
            SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};
            DataContainer<data_t> input{coeffDesc};
            DataContainer<data_t> output(
                xgiDesc, Eigen::Matrix<data_t, rangeSize * rangeSize * numOfImgs, 1>::Ones());
            axdtOp.applyAdjoint(output, input);

            THEN("results should be close to ground truth")
            {
                Vector_t<double> groundTruth(domainSize * domainSize * domainSize * 15);
                groundTruth << 1.89062, 2.83593, 4.72654, 6.61716, 10.3984, 12.289, 16.0702,
                    17.9609, 1.86265e-09, 2.79397e-09, 4.65661e-09, 6.51926e-09, 8.19564e-08,
                    9.68575e-08, 1.2666e-07, 1.41561e-07, -6.51926e-08, -9.77889e-08, -1.62981e-07,
                    -2.28174e-07, 0, 0, 0, 0, -1.20787, -1.81181, -3.01968, -4.22756, 5.70455,
                    6.74174, 8.81612, 9.85331, -1.49012e-08, -2.23517e-08, -3.72529e-08,
                    -5.21541e-08, -9.155, -10.8195, -14.1486, -15.8132, 1.56907, 2.35361, 3.92268,
                    5.49175, 1.50086, 1.77374, 2.31951, 2.59239, -3.72529e-08, -5.58794e-08,
                    -9.31323e-08, -1.30385e-07, 1.22935e-07, 1.45286e-07, 1.8999e-07, 2.12342e-07,
                    1.2666e-07, 1.8999e-07, 3.1665e-07, 4.4331e-07, 8.19564e-08, 9.68575e-08,
                    1.2666e-07, 1.41561e-07, -5.21541e-08, -7.82311e-08, -1.30385e-07, -1.82539e-07,
                    -4.91738e-07, -5.81145e-07, -7.59959e-07, -8.49366e-07, 8.9407e-08, 1.3411e-07,
                    2.23517e-07, 3.12924e-07, 4.91738e-07, 5.81145e-07, 7.59959e-07, 8.49366e-07,
                    0.270092, 0.405139, 0.675231, 0.945323, -1.20126, -1.41967, -1.85649, -2.0749,
                    2.98023e-08, 4.47035e-08, 7.45058e-08, 1.04308e-07, -1.55542, -1.83822,
                    -2.40383, -2.68663, -0.301974, -0.452961, -0.754935, -1.05691, 0.74232,
                    0.877287, 1.14722, 1.28219, 2.98023e-08, 4.47035e-08, 7.45058e-08, 1.04308e-07,
                    0.587887, 0.694775, 0.908552, 1.01544, -9.35793e-06, -1.40369e-05, -2.33948e-05,
                    -3.27528e-05, -0.454149, -0.536722, -0.701867, -0.784439;

                for (index_t i = 0; i < input.getDataDescriptor().getNumberOfCoefficients(); ++i) {
                    REQUIRE(input[i] == Approx(groundTruth[i]).epsilon(1e-3));
                }
            }
        }
    }
}

TEST_CASE_TEMPLATE("exactWeightingFunction: Compare to numerical integration", data_t, float,
                   double)
{

    srand((unsigned int) 666);

    const index_t domainSize{10};
    VolumeDescriptor volDesc{{domainSize, domainSize, 3 * domainSize}};

    auto domain = volDesc.getNumberOfCoefficientsPerDimension();

    const index_t rangeSize{domainSize};
    const index_t numOfImgs{3 * domainSize};
    IndexVector_t range{{rangeSize, rangeSize, numOfImgs}};

    RealVector_t rangeSpacing{{1.0, 1.0, 1.0}};

    geometry::SourceToCenterOfRotation s2c{100};
    geometry::CenterOfRotationToDetector c2d{50};

    // arbitrary geometry
    std::vector<Geometry> geos;
    for (index_t i = 0; i < domainSize; ++i) {
        geos.emplace_back(s2c, c2d, geometry::VolumeData3D{geometry::Size3D{domain}},
                          geometry::SinogramData3D{geometry::Size3D{range}},
                          geometry::Gamma{as<real_t>(i)});
    }
    for (index_t i = 0; i < domainSize; ++i) {
        geos.emplace_back(s2c, c2d, geometry::VolumeData3D{geometry::Size3D{domain}},
                          geometry::SinogramData3D{geometry::Size3D{range}},
                          geometry::Beta{as<real_t>(i)});
    }
    for (index_t i = 0; i < domainSize; ++i) {
        geos.emplace_back(s2c, c2d, geometry::VolumeData3D{geometry::Size3D{domain}},
                          geometry::SinogramData3D{geometry::Size3D{range}},
                          geometry::Alpha{as<real_t>(i)});
    }

    axdt::DirVecList<data_t> samplingPattern;
    samplingPattern.emplace_back(0.50748, -0.3062, 0.80543);
    samplingPattern.emplace_back(-0.3062, 0.80543, 0.50748);
    samplingPattern.emplace_back(-0.50748, 0.3062, 0.80543);
    samplingPattern.emplace_back(0.80543, 0.50748, -0.3062);
    samplingPattern.emplace_back(0.3062, 0.80543, -0.50748);
    samplingPattern.emplace_back(0.80543, -0.50748, 0.3062);
    samplingPattern.emplace_back(0.3062, -0.80543, 0.50748);
    samplingPattern.emplace_back(-0.80543, -0.50748, -0.3062);
    samplingPattern.emplace_back(-0.3062, -0.80543, -0.50748);
    samplingPattern.emplace_back(-0.80543, 0.50748, 0.3062);
    samplingPattern.emplace_back(0.50748, 0.3062, -0.80543);
    samplingPattern.emplace_back(-0.50748, -0.3062, -0.80543);
    samplingPattern.emplace_back(0.62636, -0.24353, -0.74052);
    samplingPattern.emplace_back(-0.24353, -0.74052, 0.62636);
    samplingPattern.emplace_back(-0.62636, 0.24353, -0.74052);
    samplingPattern.emplace_back(-0.74052, 0.62636, -0.24353);
    samplingPattern.emplace_back(0.24353, -0.74052, -0.62636);
    samplingPattern.emplace_back(-0.74052, -0.62636, 0.24353);
    samplingPattern.emplace_back(0.24353, 0.74052, 0.62636);
    samplingPattern.emplace_back(0.74052, -0.62636, -0.24353);
    samplingPattern.emplace_back(-0.24353, 0.74052, -0.62636);
    samplingPattern.emplace_back(0.74052, 0.62636, 0.24353);
    samplingPattern.emplace_back(0.62636, 0.24353, 0.74052);
    samplingPattern.emplace_back(-0.62636, -0.24353, 0.74052);
    samplingPattern.emplace_back(-0.28625, 0.95712, -0.044524);
    samplingPattern.emplace_back(0.95712, -0.044524, -0.28625);
    samplingPattern.emplace_back(0.28625, -0.95712, -0.044524);
    samplingPattern.emplace_back(-0.044524, -0.28625, 0.95712);
    samplingPattern.emplace_back(-0.95712, -0.044524, 0.28625);
    samplingPattern.emplace_back(-0.044524, 0.28625, -0.95712);
    samplingPattern.emplace_back(-0.95712, 0.044524, -0.28625);
    samplingPattern.emplace_back(0.044524, 0.28625, 0.95712);
    samplingPattern.emplace_back(0.95712, 0.044524, 0.28625);
    samplingPattern.emplace_back(0.044524, -0.28625, -0.95712);
    samplingPattern.emplace_back(-0.28625, -0.95712, 0.044524);
    samplingPattern.emplace_back(0.28625, 0.95712, 0.044524);

    XGIDetectorDescriptor xgiDesc{range, rangeSpacing, geos, XGIDetectorDescriptor::DirVec(1, 0, 0),
                                  true};

    WHEN("computing the weights for even symmetry")
    {
        const auto symmetry = Symmetry::even;
        const auto maxL = 4;

        auto numericalWeights =
            axdt::numericalWeightingFunction<data_t>(xgiDesc, symmetry, maxL, samplingPattern);

        auto exactWeights = axdt::exactWeightingFunction<data_t>(xgiDesc, symmetry, maxL);

        THEN("results should be close")
        {
            REQUIRE_UNARY(isCwiseApprox(numericalWeights, exactWeights));
        }
    }

    WHEN("computing the weights for regular symmetry")
    {
        const auto symmetry = Symmetry::regular;
        const auto maxL = 4;

        auto numericalWeights =
            axdt::numericalWeightingFunction<data_t>(xgiDesc, symmetry, maxL, samplingPattern);
        auto exactWeights = axdt::exactWeightingFunction<data_t>(xgiDesc, symmetry, maxL);

        THEN("results should be close")
        {
            REQUIRE_UNARY(isCwiseApprox(numericalWeights, exactWeights));
        }
    }

    WHEN("computing the weights for possible choices of lmax")
    {
        const auto symmetry = Symmetry::regular;

        for (index_t maxL = 0; maxL < 5; ++maxL) {

            auto numericalWeights =
                axdt::numericalWeightingFunction<data_t>(xgiDesc, symmetry, maxL, samplingPattern);

            auto exactWeights = axdt::exactWeightingFunction<data_t>(xgiDesc, symmetry, maxL);

            THEN("results should be close")
            {
                REQUIRE_UNARY(isCwiseApprox(numericalWeights, exactWeights));
            }
        }
    }
    WHEN("Applying AXDTOperators with numerical and analytical weights")
    {
        const auto symmetry = Symmetry::regular;
        const auto maxL = 4;

        auto numericalWeights =
            axdt::numericalWeightingFunction<data_t>(xgiDesc, symmetry, maxL, samplingPattern);
        auto exactWeights = axdt::exactWeightingFunction<data_t>(xgiDesc, symmetry, maxL);

        Identity<data_t> proj{volDesc};

        SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};

        AXDTOperator<data_t> numericalOp{coeffDesc, xgiDesc, proj, numericalWeights};
        AXDTOperator<data_t> exactOp{coeffDesc, xgiDesc, proj, exactWeights};

        auto [randomInput, unused] = generateRandomContainer<data_t>(coeffDesc);

        auto numericalOut = numericalOp.apply(randomInput);
        auto exactOut = exactOp.apply(randomInput);

        THEN("results should be close")
        {
            REQUIRE_UNARY(isCwiseApprox(numericalOut, exactOut));
        }
    }
}

TEST_SUITE_END();
