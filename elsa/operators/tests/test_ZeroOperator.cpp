/**
 * @file test_ZeroOperator.cpp
 *
 * @brief Tests for ZeroOperator class
 */

#include "doctest/doctest.h"
#include "ZeroOperator.h"
#include "VolumeDescriptor.h"
#include "IdenticalBlocksDescriptor.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("core");

TEST_CASE_TEMPLATE("ZeroOperator: Testing construction", data_t, float, double)
{
    GIVEN("a descriptor")
    {
        IndexVector_t numCoeffDomain(3);
        numCoeffDomain << 13, 45, 28;
        VolumeDescriptor dDomain(numCoeffDomain);

        IndexVector_t numCoeffRange(2);
        numCoeffRange << 13, 28;
        VolumeDescriptor dRange(numCoeffRange);
        IdenticalBlocksDescriptor ibdRange(4, dRange);

        WHEN("instantiating a ZeroOperator")
        {
            ZeroOperator<data_t> op(dDomain, ibdRange);

            THEN("the DataDescriptors are as expected")
            {
                REQUIRE(op.getDomainDescriptor() == dDomain);
                REQUIRE(op.getRangeDescriptor() == ibdRange);
            }
        }

        WHEN("cloning a ZeroOperator")
        {
            ZeroOperator<data_t> op(dDomain, ibdRange);
            auto opClone = op.clone();

            THEN("everything matches")
            {
                REQUIRE(opClone.get() != &op);
                REQUIRE(*opClone == op);
            }
        }
    }
}

TEST_CASE_TEMPLATE("ZeroOperator: Testing apply", data_t, float, double, complex<float>,
                   complex<double>)
{
    GIVEN("some data")
    {
        IndexVector_t numCoeffDomain(2);
        numCoeffDomain << 11, 13;
        VolumeDescriptor dDomain(numCoeffDomain);
        DataContainer<data_t> input(dDomain);
        input = 3.3f;

        IndexVector_t numCoeffRange(2);
        numCoeffRange << 13, 28;
        VolumeDescriptor dRange(numCoeffRange);
        IdenticalBlocksDescriptor ibdRange(4, dRange);

        ZeroOperator<data_t> op(dDomain, ibdRange);

        WHEN("applying the ZeroOperator")
        {
            auto output = op.apply(input);

            THEN("the result is as expected")
            {
                auto expected = DataContainer<data_t>(ibdRange);
                expected = 0;

                REQUIRE(output == expected);
            }
        }

        WHEN("applying the adjoint of ZeroOperator")
        {
            auto inputAdj = DataContainer<data_t>(ibdRange);
            inputAdj = 3;
            auto outputAdj = op.applyAdjoint(inputAdj);

            auto expectedAdj = DataContainer<data_t>(dDomain);
            expectedAdj = 0;

            THEN("the results is as expected")
            {
                REQUIRE(outputAdj == expectedAdj);
            }
        }
    }
}
TEST_SUITE_END();
