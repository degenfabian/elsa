#pragma once

#include "XGIDetectorDescriptor.h"
#include "elsaDefines.h"

namespace elsa::axdt
{

    /**
     * @brief
     * Compute the spherical harmonics weights for the AXDT imaging setting (i.e. the diagonal of
     * all the [W_(k,m)] matrices in "Anisotropic X-Ray Dark-Field Tomography: A Continuous Model
     * and its Discretization" (Wieczorek et al.)). These require representing the weighting
     * function h given by Malecki in the spherical harmonics basis. Instead of doing the tedious
     * group theory calculations, we can simply evaluate the function on several sampling directions
     * in direction space (two functions below) and compute the scalar product with Y_{lm}
     * (equivalent to numerical integration). As we assume eta to be even, we typically use degree
     * l=4 so #coeffs would be 15.

     * @tparam data_t
     * @param rangeDescriptor Detector descriptor describing the imaging setup.
     * @param symmetry Symmetry hint of the spherical harmonics.
     * @param degree Maximum of the spherical harmonics.
     * @param directions List of sampling directions for numerical integration.
     * @return Weight matrix with shape (detX×detY)×#poses×coeffs
     * @author Cederik Höfs
     */
    template <typename data_t>
    DataContainer<data_t> numericalWeightingFunction(const XGIDetectorDescriptor& rangeDescriptor,
                                                     const Symmetry symmetry, const index_t degree,
                                                     const DirVecList<data_t>& directions);

    /**
     * @brief
     * Analogous to numericalWeightingFunction, but we computed the exact spherical harmonical
     * coefficients of the weighting function h using Mathematica. To reproduce this, write out the
     * function h explicitly in spherical coordinates and perform the integration times
     * SphericalHarmonicY[l,m,θ,ϕ] on the unit sphere to obtain the scalar product. As Mathematica
     * uses the complex spherical harmonics, we need to linearly combine some results (amounting to
     * taking the real and imaginary parts) to get the true coefficients in our real harmonical
     * basis.
     * @author Cederik Höfs
     */
    template <typename data_t>
    DataContainer<data_t> exactWeightingFunction(const XGIDetectorDescriptor& rangeDescriptor,
                                                 const Symmetry symmetry, const index_t degree);

} // namespace elsa::axdt
