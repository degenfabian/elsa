#pragma once

#include "LineSearchMethod.h"
#include "LeastSquares.h"

namespace elsa
{
    /**
     * @brief Steepest Descent Step for Least Squares
     *
     * SteepestDescentStepLS returns the step size @f$\alpha@f$ that minimizes the objective
     * function along the descent direction using the analytical solution:
     *
     * @f[
     * \alpha = \frac{b^T A d_i - x_i^T A^T A d_i}{d_i^TA^TAd_i}
     * @f]
     *
     * where @f$A: \mathbb{R}^{n \times m} is the system matrix@f$,
     * @f$ x_i: \mathbb{R}^n is the current solution guess@f$
     * @f$b: \mathbb{R}^m data to use in the linear residual@f$, and
     * @f$ d_i: \mathbb{R}^n is the search direction @f$
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class SteepestDescentStepLS : public LineSearchMethod<data_t>
    {
    public:
        SteepestDescentStepLS(const LeastSquares<data_t>& problem);
        ~SteepestDescentStepLS() override = default;

        /// make copy constructor deletion explicit
        SteepestDescentStepLS(const SteepestDescentStepLS<data_t>&) = delete;

        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;

    private:
        // the system matrix
        std::unique_ptr<LinearOperator<data_t>> _A{};

        // the projections vector (sinogram)
        DataContainer<data_t> _b{};

        /// implement the polymorphic clone operation
        SteepestDescentStepLS<data_t>* cloneImpl() const override;
    };
} // namespace elsa
