#include "GoldsteinCondition.h"
#include "utils/utils.h"

namespace elsa
{

    template <typename data_t>
    GoldsteinCondition<data_t>::GoldsteinCondition(const Functional<data_t>& problem, data_t amax,
                                                   data_t c, index_t max_iterations)
        : LineSearchMethod<data_t>(problem, max_iterations), _amax(amax), _c(c)
    {
        // sanity checks
        if (amax <= 0)
            throw InvalidArgumentError("GoldsteinCondition: amax has to be greater than 0");
        if (c <= 0 or c >= 0.5)
            throw InvalidArgumentError("GoldsteinCondition: c has to be in the range (0,0.5)");
    }

    template <typename data_t>
    data_t GoldsteinCondition<data_t>::_zoom(data_t a_lo, data_t a_hi, data_t f_lo, data_t f_hi,
                                             data_t f0, data_t der_f_lo, data_t der_f0,
                                             const DataContainer<data_t>& xi,
                                             const DataContainer<data_t>& di,
                                             index_t max_iterations)
    {
        data_t aj = 0;
        data_t fj = f0;
        data_t a_min = 0;
        for (index_t i = 0; i < max_iterations; ++i) {
            data_t cchk = static_cast<data_t>(0.2) * (a_hi - a_lo);
            if (i > 0) {
                a_min = cubic_interpolation<data_t>(a_lo, f_lo, der_f_lo, a_hi, f_hi, aj, fj);
            }
            if (i == 0 or std::isnan(a_min) or (a_min > a_hi - cchk) or (a_min < a_lo + cchk)) {
                a_min = a_lo + static_cast<data_t>(0.5) * (a_hi - a_lo);
            }
            data_t f_min = this->_problem->evaluate(xi + a_min * di);
            if ((f_min > f0 + _c * a_min * der_f0) or f_min >= f_lo) {
                aj = a_hi;
                a_hi = a_min;
                fj = f_hi;
                f_hi = f_min;
            } else {
                if (f_min <= f0 + (1 - _c) * a_min * der_f0) {
                    return a_min;
                }
                data_t der_f_min = di.dot(this->_problem->getGradient(xi + a_min * di));
                if (der_f_min * (a_hi - a_lo) >= 0) {
                    aj = a_hi;
                    a_hi = a_lo;
                    fj = f_hi;
                    f_hi = f_lo;
                } else {
                    aj = a_lo;
                    fj = f_lo;
                }
                a_lo = a_min;
                f_lo = f_min;
                der_f_lo = der_f_min;
            }
        }
        return a_min;
    }

    template <typename data_t>
    data_t GoldsteinCondition<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        data_t ai_1 = 0;
        auto f0 = this->_problem->evaluate(xi);
        auto der_f0 = di.dot(this->_problem->getGradient(xi));
        auto fi_1 = f0;
        auto der_fi_1 = der_f0;
        auto ai = std::min(static_cast<data_t>(1.0), _amax);
        for (index_t i = 0; i < this->_max_iterations; ++i) {
            auto fi = this->_problem->evaluate(xi + ai * di);
            auto der_fi = di.dot(this->_problem->getGradient(xi + ai * di));
            if ((fi > f0 + _c * ai * der_f0) or (fi >= fi_1 and i > 0)) {
                return _zoom(ai_1, ai, fi_1, fi, f0, der_fi_1, der_f0, xi, di);
            }
            if (fi >= f0 + (1 - _c) * ai * der_f0) {
                return ai;
            }
            if (der_fi >= 0) {
                return _zoom(ai, ai_1, fi, fi_1, f0, der_fi, der_f0, xi, di);
            }
            auto a = 2 * ai;
            ai_1 = ai;
            ai = std::min(a, _amax);
            fi_1 = fi;
            der_fi_1 = der_fi;
        }
        return ai;
    }

    template <typename data_t>
    GoldsteinCondition<data_t>* GoldsteinCondition<data_t>::cloneImpl() const
    {
        return new GoldsteinCondition(*this->_problem, _amax, _c, this->_max_iterations);
    }

    template <typename data_t>
    bool GoldsteinCondition<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto otherGC = downcast_safe<GoldsteinCondition<data_t>>(&other);
        if (!otherGC)
            return false;

        return (_amax == otherGC->_amax && _c == otherGC->_c);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class GoldsteinCondition<float>;
    template class GoldsteinCondition<double>;
} // namespace elsa
