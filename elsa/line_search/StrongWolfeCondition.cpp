#include "StrongWolfeCondition.h"
#include "utils/utils.h"

namespace elsa
{

    template <typename data_t>
    StrongWolfeCondition<data_t>::StrongWolfeCondition(const Functional<data_t>& problem,
                                                       data_t amax, data_t c1, data_t c2,
                                                       index_t max_iterations)
        : LineSearchMethod<data_t>(problem, max_iterations), _amax(amax), _c1(c1), _c2(c2)
    {
        // sanity checks
        if (amax <= 0)
            throw InvalidArgumentError("StrongWolfeCondition: amax has to be greater than 0");
        if (c1 <= 0 or c1 >= 1.0)
            throw InvalidArgumentError("StrongWolfeCondition: c1 has to be in the range (0,1)");
        if (c2 <= c1 or c2 >= 1.0)
            throw InvalidArgumentError("StrongWolfeCondition: c2 has to be in the range (c1,1)");
    }

    template <typename data_t>
    data_t StrongWolfeCondition<data_t>::_zoom(data_t a_lo, data_t a_hi, data_t f_lo, data_t f_hi,
                                               data_t f0, data_t der_f_lo, data_t der_f0,
                                               const DataContainer<data_t>& xi,
                                               const DataContainer<data_t>& di,
                                               index_t max_iterations)
    {
        data_t aj = 0;
        auto fj = f0;
        data_t amin = 0;
        data_t dalpha, a, b, cchk;
        for (index_t i = 0; i < max_iterations; ++i) {
            dalpha = a_hi - a_lo;
            if (dalpha < 0) {
                a = a_hi;
                b = a_lo;
            } else {
                a = a_lo;
                b = a_hi;
            }
            cchk = 0.2 * dalpha;
            // TODO: check if interpolation is successful
            if (i > 0) {
                amin = cubic_interpolation(a_lo, f_lo, der_f_lo, a_hi, f_hi, aj, fj);
            }
            if (i == 0 or std::isnan(amin) or amin > b - cchk or amin < a + cchk) {
                amin = a_lo + 0.5 * dalpha;
            }
            auto fmin = this->_problem->evaluate(xi + amin * di);
            if ((fmin > f0 + _c1 * amin * der_f0) or fmin >= f_lo) {
                aj = a_hi;
                a_hi = amin;
                fj = f_hi;
                f_hi = fmin;
            } else {
                auto der_fmin = di.dot(this->_problem->getGradient(xi + amin * di));
                if (std::abs(der_fmin) <= -_c2 * der_f0) {
                    return amin;
                }
                if (der_fmin * dalpha >= 0) {
                    aj = a_hi;
                    a_hi = a_lo;
                    fj = f_hi;
                    f_hi = f_lo;
                } else {
                    aj = a_lo;
                    fj = f_lo;
                }
                a_lo = amin;
                f_lo = fmin;
                der_f_lo = der_fmin;
            }
        }
        return amin;
    }

    template <typename data_t>
    data_t StrongWolfeCondition<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        data_t ai_1 = 0;
        data_t f0 = this->_problem->evaluate(xi);
        data_t der_f0 = di.dot(this->_problem->getGradient(xi));
        auto fi_1 = f0;
        auto der_fi_1 = der_f0;
        auto ai = std::min(static_cast<data_t>(1.0), _amax);
        for (index_t i = 0; i < this->_max_iterations; ++i) {
            auto fi = this->_problem->evaluate(xi + ai * di);
            if ((fi > f0 + _c1 * ai * der_f0) or (fi >= fi_1 and i > 0)) {
                return _zoom(ai_1, ai, fi_1, fi, f0, der_fi_1, der_f0, xi, di);
            }
            auto der_fi = di.dot(this->_problem->getGradient(xi + ai * di));
            if (std::abs(der_fi) <= _c2 * std::abs(der_f0)) {
                return ai;
            }
            if (der_fi >= 0) {
                return _zoom(ai, ai_1, fi, fi_1, f0, der_fi, der_f0, xi, di);
            }
            ai_1 = ai;
            ai = std::min(2 * ai, _amax);
            fi_1 = fi;
            der_fi_1 = der_fi;
        }
        return ai;
    }

    template <typename data_t>
    StrongWolfeCondition<data_t>* StrongWolfeCondition<data_t>::cloneImpl() const
    {
        return new StrongWolfeCondition(*this->_problem, _amax, _c1, _c2, this->_max_iterations);
    }

    template <typename data_t>
    bool StrongWolfeCondition<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto otherSWC = downcast_safe<StrongWolfeCondition<data_t>>(&other);
        if (!otherSWC)
            return false;

        return (_amax == otherSWC->_amax && _c1 == otherSWC->_c1 && _c2 == otherSWC->_c2);
    }
    // ------------------------------------------
    // explicit template instantiation
    template class StrongWolfeCondition<float>;
    template class StrongWolfeCondition<double>;
} // namespace elsa
