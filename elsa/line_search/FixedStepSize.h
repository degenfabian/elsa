#pragma once

#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief Fixed Step Size
     *
     * Simply returns a fixed step size.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class FixedStepSize : public LineSearchMethod<data_t>
    {
    public:
        FixedStepSize(const Functional<data_t>& problem, data_t step_size = 1);
        ~FixedStepSize() override = default;

        /// make copy constructor deletion explicit
        FixedStepSize(const FixedStepSize<data_t>&) = delete;

        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;

    private:
        // the fixed step size
        data_t _step_size;

        /// implement the polymorphic clone operation
        FixedStepSize<data_t>* cloneImpl() const override;
    };
} // namespace elsa
