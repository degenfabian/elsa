#include "doctest/doctest.h"

#include "ArmijoCondition.h"
#include "Identity.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("line_search");
TYPE_TO_STRING(ArmijoCondition<float>);
TYPE_TO_STRING(ArmijoCondition<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("ArmijoCondition: Solving a simple problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a simple problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 2;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();
        DataContainer dcB(dd, bVec);

        Vector_t<data_t> randomData(dd.getNumberOfCoefficients());
        randomData.setRandom();
        DataContainer<data_t> scaleFactors(dd, randomData * static_cast<data_t>(5.14));

        Scaling<data_t> scalingOp(scaleFactors);
        LeastSquares<data_t> prob(scalingOp, dcB);

        WHEN("setting up a ArmijoCondition Line Search and 0s for an initial guess")
        {
            ArmijoCondition<data_t> line_search{prob, 2, 0.1, 0.5};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 0;
                    std::array<data_t, 10> solutions{
                        0.0625, 0.0625, 0.0625, 0.0625, 0.25, 0.0625, 0.125, 0.0625, 0.125, 0.0625,
                    };

                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }

        WHEN("setting up ArmijoCondition line search and 1s for an intial guess")
        {
            ArmijoCondition<data_t> line_search{prob, 2, 0.1, 0.5};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 1;
                    std::array<data_t, 10> solutions{
                        0.0625, 0.0625, 0.0625, 0.0625, 0.125, 0.0625, 0.125, 0.125, 0.0625, 0.125,
                    };

                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }
    }
}
TEST_SUITE_END();
