# GPU-accelarated FFT
## Basic usage
GPU-accelerated FFT is implemented via the cuFFT library.
elsa provides FFT operations under the following interface:
```
template <typename data_t>
void fft(ContiguousStorage<data_t>& x,
    const DataDescriptor& desc, FFTNorm norm,
    FFTPolicy policy = FFTPolicy::AUTO);

template <typename data_t>
void ifft(ContiguousStorage<data_t>& x,
    const DataDescriptor& desc, FFTNorm norm,
    FFTPolicy policy = FFTPolicy::AUTO);

template <typename data_t>
DataContainer<complex<data_t>> rfft(
    const DataContainer<data_t>& dc, FFTNorm norm,
    FFTPolicy policy = FFTPolicy::AUTO);

template <typename data_t>
DataContainer<data_t> irfft(
    const DataContainer<complex<data_t>>& dc, FFTNorm norm,
    FFTPolicy policy = FFTPolicy::AUTO);
```

Note that the `fft` and `ifft` functions do not return a value and take a mutable reference to contiguous storage, while the `rfft` and `irfft` take a constant reference to a `DataContainer` and return owned data. This is because the `fft` and `ifft` functions are typically not called directly, but instead called through `DataContainer<data_t>::fft` and the corresponding inverse transform, which also take a norm and policy as arguments. For the R2C and C2R transforms, this is not possible, as the transformed container stores data of a different type than the input. Similarly, the python bindings for the FFT functionality defines the `fft` and `ifft` functionality on the `DataContainer` object and the `rfft` and `irfft` transforms on the module. The python bindings also include an optional policy parameter. For both the python bindings and the `C++` library, the default policy `FFTPolicy::AUTO` chooses the device implementation if available. The host implementation is used if GPU acceleration is unavailable or if the device function fails for any reason. If you want to force the transform to run on the CPU/GPU, use `FFTPolicy::HOST`/`FFTPolicy::DEVICE`.

## Configuration Options
There is a new build config option with a performance optimization related to cuFFT plans.
The cuFFt library's API splits a transform into plan creation and execution. The plan contains basic information about the transform, such as the dimensions and layout of the input, as well as the type of the transformation. Additional internal state attached to the plan allows transforms to be quickly dispatched and executed, including a work area used to store temporary results. The creation of a plan is an expensive operation. It is therefore beneficial to reduce the number of plan creation operations by reusing plans for multiple executions. Conversely, the number of simultaneously existing plans should also be kept low in order to reduce pressure on device memory and avoid performance degradation.

The config option `ELSA_CUFFT_CACHE_SIZE` controls how many cuFFT plans are cached for reuse. When full, the cache always evicts the least recently used plan. Plans can only be reused when the dimensions, operation and data type match exactly, for example in a loop that performs the same transform over and over again. A cache size of 1 is recommended (and used by default), but if you notice that your application is using excessive amounts of GPU memory, then it might be worth setting the cache size to 0. Larger cache sizes would only be benefitial if you alternatingly perform FFTs on different data sizes.

## Performance
The graphs below compare the performance of the cuFFT back-end with the performance of the CPU back-end. The CPU-only build is configured to use the FFTW library (which is the default when the library is present). The performance measurements also include the rocFFT backend on AMD hardware, which was an experiment that is unlikely to be merged at this stage.

Benchmarks for the CPU or CUDA platform were executed on a machine with an AMD EPYC 7452 32-Core Processor with a 2.35GHz base frequency, 504GB of RAM and a Quadro RTX 6000 GPU.
The AMD platform benchmarks are on an Intel Core i7 CPU with 8 cores and a base frequency of 4GHz, 32GB of RAM and a Radeon VII GPU.

### float 1d transform
<img src="images/cufft_float_1d.png" style="width:300px;"/>

### double 1d transform
<img src="images/cufft_double_1d.png" style="width:300px;"/>

### float 2d transform
<img src="images/cufft_float_2d.png" style="width:300px;"/>

### double 2d transform
<img src="images/cufft_double_2d.png" style="width:300px;"/>

### float 3d transform
<img src="images/cufft_float_3d.png" style="width:300px;"/>

### double 3d transform
<img src="images/cufft_double_3d.png" style="width:300px;"/>
