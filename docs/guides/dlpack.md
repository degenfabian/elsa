# Exchanging data via dlpack
To support interoperability between the elsa framework and other large frameworks (like `numpy` or `tensorflow`), the `dlpack` standard was integrated into the python bindings of elsa. The `dlpack` standard is an interface for tensor data exchange between multiple frameworks, which implement the `dlpack` methods as well.

## Usage

### Export
Data from `elsa` can be easily exported to other frameworks as follows:
```
dc = elsa.DataContainer(elsa.VolumeDescriptor([2, 3]), [1, 4, 2, 5, 3, 6])
arr = np.from_dlpack(dc)
```
In this example, `numpy` creates a read-only view of the tensor data without copying. More on this [here](https://numpy.org/devdocs/user/basics.interoperability.html).

### Import
Conversely, data from other frameworks can be imported like so:
```
arr = np.array([[1, 2, 3], [4, 5, 6]])
dc = elsa.from_dlpack(arr)
```
In principle, this method of data import works for any framwork that adheres to the `dlpack`-specification, i.e. provides `__dlpack__()` and `__dlpack_device__()` on its tensor objects.  

### Data Ownership
In order to facilitate zero-copy import/export, the `DataContainer` can handle shared ownership. It reference counts views on foreign tensor data and notifies the owning framework, once an import is no longer referenced. Likewise, it guarantees that exported views remain valid until the foreign framework releases its reference. 

## Restrictions

### Incompatibilities
1. `numpy` does not allow the user to export read-only tensors, as this cannot be communicated via `dlpack`
2. The importing framework must be able to handle the storage type of the tensor data (e.g. CPU memory, CUDA unified memory, CUDA device memory, ROCm memory, etc.). The `elsa` framework can handle CPU memory (and CUDA memories if compiled with CUDA support)

### Copying
The `elsa` framework currently runs all of its algorithms on a specific storage type (CPU memory or unified memory, depending on whether CUDA is enabled). Hence, the `DataContainer` class cannot handle any other storage type. Similarly, `DataContainer` assumes contiguous, row-major data. If the exporting framework does not adhere to these requirements, `elsa` cannot import the data without copying. Note that, although copying is discouraged by the `dlpack`-specification, it is allowed when zero-copy import is not possible. The result of importing via `dlpack` must be a `DataContainer`, because it is currently the only supported data type by most `elsa` algorithms. Therefore, a copy is performed on import if the layout/storage does not match.

The `NdView` class exists to alleviate these issues. It can handle near arbitrary data layouts and allows data of different storage types. Unforunately, the `NdView` is not yet integrated with most algorithms. Once the `NdView` is properly integrated, all data imports should be possible without copying. 

