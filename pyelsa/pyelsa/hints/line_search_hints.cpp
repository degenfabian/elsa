#include "hints_base.h"

#ifndef ELSA_BINDINGS_IN_SINGLE_MODULE
#include "Logger.h"
#endif

namespace elsa
{
    namespace py = pybind11;

#ifndef ELSA_BINDINGS_IN_SINGLE_MODULE
    class LineSearchHints : ModuleHints
    {

        struct LoggerLineSearch : Logger {
        };

    public:
        static void addCustomFunctions(py::module& m)
        {
            // expose Logger class
            py::class_<LoggerLineSearch>(m, "logger_pyelsa_line_search")
                .def_static("setLevel", &LoggerLineSearch::setLevel)
                .def_static("enableFileLogging", &LoggerLineSearch::enableFileLogging)
                .def_static("flush", &LoggerLineSearch::flush);
        }
    };
#endif
} // namespace elsa
