#include <pybind11/pybind11.h>
#include <pybind11/complex.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "BlockLinearOperator.h"
#include "Dictionary.h"
#include "FiniteDifferences.h"
#include "FourierTransform.h"
#include "FunkRadonTransform.h"
#include "SphericalFunctionTransform.h"
#include "Identity.h"
#include "Scaling.h"
#include "Diagonal.h"
#include "ShearletTransform.h"
#include "AXDTOperator.h"
#include "XGIDetectorDescriptor.h"
#include "SphericalCoefficientsDescriptor.h"
#include "ZeroOperator.h"
#include "SymmetrizedDerivative.h"
#include "WeightingFunction.h"
#include "Math.hpp"
#include "SphericalPositivity.h"

#include "bind_common.h"

#include "hints/operators_hints.cpp"

namespace py = pybind11;

namespace detail
{
    template <class data_t>
    void add_finite_diff_difftype(py::module& m, const char* name)
    {
        using Op = elsa::FiniteDifferences<data_t>;
        py::enum_<typename Op::DiffType> e(m, name);

        e.value("BACKWARD", Op::DiffType::BACKWARD);
        e.value("CENTRAL", Op::DiffType::CENTRAL);
        e.value("FORWARD", Op::DiffType::FORWARD);
    }
} // namespace detail

void add_finite_difference_difftype(py::module& m)
{
    detail::add_finite_diff_difftype<float>(m, "FiniteDifferencesfDiffType");
    detail::add_finite_diff_difftype<double>(m, "FiniteDifferencesfDiffTyped");
    detail::add_finite_diff_difftype<thrust::complex<float>>(m, "FiniteDifferencesfDiffTypecf");
    detail::add_finite_diff_difftype<thrust::complex<double>>(m, "FiniteDifferencesfDiffTypecd");
}

namespace detail
{
    template <class data_t>
    void add_identity_op(py::module& m, const char* name)
    {
        py::class_<elsa::Identity<data_t>, elsa::LinearOperator<data_t>> op(m, name);
        op.def(py::init<const elsa::DataDescriptor&>(), py::arg("descriptor"));
        op.def("set",
               py::overload_cast<const elsa::Identity<data_t>&>(&elsa::Identity<data_t>::operator=),
               py::return_value_policy::reference_internal);
    }
} // namespace detail

void add_identity(py::module& m)
{
    detail::add_identity_op<float>(m, "Identityf");
    detail::add_identity_op<double>(m, "Identityd");
    detail::add_identity_op<thrust::complex<float>>(m, "Identitycf");
    detail::add_identity_op<thrust::complex<double>>(m, "Identitycd");

    m.attr("Identity") = m.attr("Identityf");
}

namespace detail
{
    template <class data_t>
    void add_diagonal_op(py::module& m, const char* name)
    {
        py::class_<elsa::Diagonal<data_t>, elsa::LinearOperator<data_t>> op(m, name);
        op.def(py::init([](const elsa::DataDescriptor& domain, const elsa::DataDescriptor& range,
                           std::vector<const elsa::LinearOperator<data_t>*>& opList) {
            std::vector<std::unique_ptr<elsa::LinearOperator<data_t>>> ops;
            ops.reserve(opList.size());
            for (const auto op : opList) {
                ops.push_back(op->clone());
            }
            return std::make_unique<elsa::Diagonal<data_t>>(domain, range, ops);
        }));
    }
} // namespace detail

void add_diagonal(py::module& m)
{
    detail::add_diagonal_op<float>(m, "Diagonalf");
    detail::add_diagonal_op<double>(m, "Diagonald");

    m.attr("Diagonal") = m.attr("Diagonalf");
}

namespace detail
{
    template <class data_t>
    void add_zero_operator_op(py::module& m, const char* name)
    {
        using Op = elsa::ZeroOperator<data_t>;
        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def(py::init<const elsa::DataDescriptor&, const elsa::DataDescriptor&>(),
               py::arg("domainDescriptor"), py::arg("rangeDescriptor"));
    }
} // namespace detail

void add_zero_operator(py::module& m)
{
    detail::add_zero_operator_op<float>(m, "ZeroOperatorf");
    detail::add_zero_operator_op<double>(m, "ZeroOperatord");
    detail::add_zero_operator_op<thrust::complex<float>>(m, "ZeroOperatorcf");
    detail::add_zero_operator_op<thrust::complex<double>>(m, "ZeroOperatorcd");

    m.attr("ZeroOperator") = m.attr("ZeroOperatorf");
}

namespace detail
{
    template <class data_t>
    void add_scaling_op(py::module& m, const char* name)
    {
        using Op = elsa::Scaling<data_t>;
        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def("isIsotropic", py::overload_cast<>(&Op::isIsotropic, py::const_));
        op.def("getScaleFactors", py::overload_cast<>(&Op::getScaleFactors, py::const_),
               py::return_value_policy::reference_internal);
        op.def("getScaleFactor", py::overload_cast<>(&Op::getScaleFactor, py::const_));
        op.def(py::init<const elsa::DataDescriptor&, data_t>(), py::arg("descriptor"),
               py::arg("scaleFactor"));
        op.def(py::init<const elsa::DataContainer<data_t>&>(), py::arg("scaleFactors"));
    }
} // namespace detail

void add_scaling(py::module& m)
{
    detail::add_scaling_op<float>(m, "Scalingf");
    detail::add_scaling_op<double>(m, "Scalingd");
    detail::add_scaling_op<thrust::complex<float>>(m, "Scalingcf");
    detail::add_scaling_op<thrust::complex<double>>(m, "Scalingcd");

    m.attr("Scaling") = m.attr("Scalingf");
}

namespace detail
{
    template <class data_t>
    void add_finite_diff_op(py::module& m, const char* name)
    {
        using Op = elsa::FiniteDifferences<data_t>;
        using BoolVector = Eigen::Matrix<bool, Eigen::Dynamic, 1>;

        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def("set", py::overload_cast<const Op&>(&Op::operator=),
               py::return_value_policy::reference_internal);
        op.def(py::init<const elsa::DataDescriptor&, const BoolVector&>(),
               py::arg("domainDescriptor"), py::arg("activeDims"));
        op.def(py::init<const elsa::DataDescriptor&, const BoolVector&, typename Op::DiffType>(),
               py::arg("domainDescriptor"), py::arg("activeDims"), py::arg("type"));
        op.def(py::init<const elsa::DataDescriptor&>(), py::arg("domainDescriptor"));
        op.def(py::init<const elsa::DataDescriptor&, typename Op::DiffType>(),
               py::arg("domainDescriptor"), py::arg("type"));
    }
} // namespace detail

void add_finite_difference(py::module& m)
{
    detail::add_finite_diff_op<float>(m, "FiniteDifferencesf");
    detail::add_finite_diff_op<double>(m, "FiniteDifferencesd");
    detail::add_finite_diff_op<thrust::complex<float>>(m, "FiniteDifferencescf");
    detail::add_finite_diff_op<thrust::complex<double>>(m, "FiniteDifferencescd");

    m.attr("FiniteDifferences") = m.attr("FiniteDifferencesf");
}

namespace detail
{
    template <class data_t>
    void add_symm_der_op(py::module& m, const char* name)
    {
        using Op = elsa::SymmetrizedDerivative<data_t>;

        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def(py::init<const elsa::DataDescriptor&>(), py::arg("domainDescriptor"));
    }
} // namespace detail

void add_symm_der(py::module& m)
{
    detail::add_symm_der_op<float>(m, "SymmetrizedDerivativef");
    detail::add_symm_der_op<double>(m, "SymmetrizedDerivatived");

    m.attr("SymmetrizedDerivative") = m.attr("SymmetrizedDerivativef");
}

namespace detail
{
    template <class data_t>
    void add_fourier_op(py::module& m, const char* name)
    {
        using Op = elsa::FourierTransform<data_t>;

        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def("set", py::overload_cast<const Op&>(&Op::operator=),
               py::return_value_policy::reference_internal);
        op.def(py::init<const elsa::DataDescriptor&, elsa::FFTNorm>(), py::arg("domainDescriptor"),
               py::arg("norm") = static_cast<elsa::FFTNorm>(2));
        op.def(py::init<const elsa::FourierTransform<data_t>&>());
    }
} // namespace detail

void add_fourier_transform(py::module& m)
{
    detail::add_fourier_op<thrust::complex<float>>(m, "FourierTransformcf");
    detail::add_fourier_op<thrust::complex<double>>(m, "FourierTransformcd");

    m.attr("FourierTransform") = m.attr("FourierTransformcf");
}

namespace detail
{
    template <class data_t>
    void add_block_op(py::module& m, const char* name)
    {
        using Op = elsa::BlockLinearOperator<data_t>;

        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def("getIthOperator", py::overload_cast<long>(&Op::getIthOperator, py::const_),
               py::arg("index"), py::return_value_policy::reference_internal);
        op.def("numberOfOps", py::overload_cast<>(&Op::numberOfOps, py::const_));

        py::enum_<typename elsa::BlockLinearOperator<data_t>::BlockType> opEnum(op, "BlockType");
        opEnum.value("COL", elsa::BlockLinearOperator<data_t>::BlockType::COL);
        opEnum.value("ROW", elsa::BlockLinearOperator<data_t>::BlockType::ROW);
        opEnum.export_values();

        elsa::BlockLinearOperatorHints<data_t>::addCustomMethods(op);
    }
} // namespace detail

void add_block_op(py::module& m)
{
    detail::add_block_op<float>(m, "BlockLinearOperatorf");
    detail::add_block_op<double>(m, "BlockLinearOperatord");
    detail::add_block_op<thrust::complex<float>>(m, "BlockLinearOperatorcf");
    detail::add_block_op<thrust::complex<double>>(m, "BlockLinearOperatorcd");

    m.attr("BlockLinearOperator") = m.attr("BlockLinearOperatorf");
}

namespace detail
{
    template <class data_t>
    void add_dictionary_op(py::module& m, const char* name)
    {
        using Op = elsa::Dictionary<data_t>;
        using IndexVector_t = elsa::IndexVector_t;

        py::class_<Op, elsa::LinearOperator<data_t>> op(m, name);
        op.def("getSupportedDictionary",
               py::overload_cast<IndexVector_t>(&Op::getSupportedDictionary, py::const_),
               py::arg("support"), py::return_value_policy::move);
        op.def("getAtom", py::overload_cast<long>(&Op::getAtom, py::const_), py::arg("j"),
               py::return_value_policy::move);
        op.def("getNumberOfAtoms", py::overload_cast<>(&Op::getNumberOfAtoms, py::const_));
        op.def(py::init<const elsa::DataContainer<data_t>&>(), py::arg("dictionary"));
        op.def(py::init<const elsa::DataDescriptor&, long>(), py::arg("signalDescriptor"),
               py::arg("nAtoms"));
        op.def("updateAtom",
               py::overload_cast<long, const elsa::DataContainer<data_t>&>(&Op::updateAtom),
               py::arg("j"), py::arg("atom"));
    }
} // namespace detail

void add_dictionary(py::module& m)
{
    detail::add_dictionary_op<float>(m, "Dictionaryf");
    detail::add_dictionary_op<double>(m, "Dictionaryd");

    m.attr("Dictionary") = m.attr("Dictionaryf");
}

namespace detail
{
    template <class data_x_t, class data_y_t>
    void add_shearlet_op(py::module& m, const char* name)
    {
        using Op = elsa::ShearletTransform<data_x_t, data_y_t>;
        using IndexVector_t = elsa::IndexVector_t;

        py::class_<Op, elsa::LinearOperator<data_x_t>> op(m, name);

        op.def("isSpectraComputed", py::overload_cast<>(&Op::isSpectraComputed, py::const_));
        op.def("getSpectra", py::overload_cast<>(&Op::getSpectra, py::const_),
               py::return_value_policy::move);
        op.def("sumByLastAxis",
               py::overload_cast<elsa::DataContainer<thrust::complex<data_y_t>>>(&Op::sumByLastAxis,
                                                                                 py::const_),
               py::arg("dc"), py::return_value_policy::move);
        op.def("getHeight", py::overload_cast<>(&Op::getHeight, py::const_));
        op.def("getNumOfLayers", py::overload_cast<>(&Op::getNumOfLayers, py::const_));
        op.def("getWidth", py::overload_cast<>(&Op::getWidth, py::const_));
        op.def("computeSpectra", py::overload_cast<>(&Op::computeSpectra, py::const_));
        op.def(py::init<IndexVector_t>(), py::arg("spatialDimensions"));
        op.def(py::init<const Op&>());
        op.def(py::init<long, long>(), py::arg("width"), py::arg("height"));
        op.def(py::init<long, long, long>(), py::arg("width"), py::arg("height"),
               py::arg("numOfScales"));
        op.def(py::init<long, long, long, std::optional<elsa::DataContainer<data_y_t>>>(),
               py::arg("width"), py::arg("height"), py::arg("numOfScales"), py::arg("spectra"));
        op.def("set", py::overload_cast<const Op&>(&Op::operator=),
               py::return_value_policy::reference_internal);
    }
} // namespace detail

void add_shearlet_operator(py::module& m)
{
    detail::add_shearlet_op<float, float>(m, "ShearletTransformff");
    detail::add_shearlet_op<thrust::complex<float>, float>(m, "ShearletTransformcff");
    detail::add_shearlet_op<double, double>(m, "ShearletTransformdd");
    detail::add_shearlet_op<thrust::complex<double>, double>(m, "ShearletTransformcdd");

    m.attr("ShearletTransform") = m.attr("ShearletTransformff");
}

void add_axdt_operator(py::module& a)
{
    using namespace elsa;
    using namespace elsa::axdt;

    using SphCoeffsDesc = SphericalCoefficientsDescriptor;

    py::class_<AXDTOperator<float>, LinearOperator<float>>(a, "AXDTOperatorf")
        .def(py::init<const SphCoeffsDesc&, const XGIDetectorDescriptor&,
                      const LinearOperator<float>&, std::optional<DataContainer<float>>>(),
             py::arg("domainDescriptor"), py::arg("rangeDescriptor"), py::arg("projector"),
             py::arg("weights") = std::nullopt);

    py::class_<AXDTOperator<double>, LinearOperator<double>>(a, "AXDTOperatord")
        .def(py::init<const SphCoeffsDesc&, const XGIDetectorDescriptor&,
                      const LinearOperator<double>&, std::optional<DataContainer<double>>>(),
             py::arg("domainDescriptor"), py::arg("rangeDescriptor"), py::arg("projector"),
             py::arg("weights") = std::nullopt);

    a.attr("AXDTOperator") = a.attr("AXDTOperatorf");
}

namespace detail
{
    using namespace elsa;
    using namespace elsa::axdt;

    using Edge = std::pair<index_t, index_t>;

    template <typename data_t>
    inline std::vector<bool> localMaxima(const DataContainer<data_t>& eval, const index_t volIdx,
                                         const std::vector<Edge>& edges)
    {
        // All nodes are initialized as local maxima
        std::vector<bool> isMax(eval.getNumberOfBlocks(), true);

        // Whenever there is (i, j) in E with eta(i) < eta(j), then i is not a local maximum
        for (auto E : edges) {
            index_t i = E.first;
            index_t j = E.second;

            float etaI = eval.getBlock(i)[volIdx];
            float etaJ = eval.getBlock(j)[volIdx];

            if (etaI < etaJ) {
                isMax[i] = false;
            } else if (etaI > etaJ) {
                isMax[j] = false;
            }
        }

        return isMax;
    }

    template <typename data_t>
    inline std::vector<index_t> getSortedIndices(const DataContainer<data_t>& eval,
                                                 const index_t volIdx,
                                                 const std::vector<bool>& isMax)
    {
        std::vector<index_t> indices;
        for (index_t i = 0; i < asSigned(isMax.size()); i++) {
            if (isMax[i]) {
                indices.push_back(i);
            }
        }

        // Sort indices descending by magnitude
        std::sort(indices.begin(), indices.end(), [&eval, volIdx](index_t a, index_t b) {
            return eval.getBlock(a)[volIdx] > eval.getBlock(b)[volIdx];
        });

        return indices;
    }

    template <typename data_t>
    inline std::vector<index_t> getUniqueIndices(const std::vector<index_t>& indices,
                                                 const axdt::DirVecList<data_t>& directions)
    {

        // Remove duplicates based on angle heuristic
        // We discard any vertices closer than 10deg to another point already added
        // TODO: Seems arbitrary to me!
        // Also we restrict ourselves to the upper hemisphere as scattering is symmetric under
        // parity
        constexpr float cos10deg = 0.984807753;

        std::vector<index_t> uniqueIndices;

        // TODO: Doesn't this induce a bias depending on the order of given sampling directions?
        // Maybe just exclude neighbouring directions?
        for (auto i : indices) {
            if (std::none_of(uniqueIndices.begin(), uniqueIndices.end(), [&](index_t j) {
                    auto cosTheta = directions[j].dot(directions[i]);
                    return abs(cosTheta) > cos10deg; // cos decreases monotonically
                })) {
                uniqueIndices.push_back(i);
            }
        }
        return uniqueIndices;
    }

    // Generalized Fractional Anisotropy represents a quality measure
    // cf. J. Cohen-Adad, M. Descoteaux, S. Rossignol, R. D. Hoge, R. Deriche, and H. Benali.
    // “Detection of multiple pathways in the spinal cord using q-ball imaging”.
    // DOI: 10.1016/j.neuroimage.2008.04.243
    // URL: http://linkinghub.elsevier.com/retrieve/pii/S1053811908005119.
    // TODO: Maybe rewrite this using thrust reductions/transforms
    template <typename data_t>
    DataContainer<data_t> gfa(const DataContainer<data_t>& sphericalCoeffs)
    {

        auto gfa = emptylike<data_t>(sphericalCoeffs.getBlock(0));

        auto volumeSize = sphericalCoeffs.getBlock(0).getSize();

#pragma omp parallel for
        for (index_t volIdx = 0; volIdx < volumeSize; volIdx++) {

            Eigen::Map<const Vector_t<data_t>, Eigen::Unaligned, Eigen::InnerStride<Eigen::Dynamic>>
                viewCoeffs{thrust::raw_pointer_cast(sphericalCoeffs.storage().data()),
                           sphericalCoeffs.getNumberOfBlocks(), volumeSize};

            gfa[volIdx] = std::sqrt(1 - viewCoeffs[0] * viewCoeffs[0] / viewCoeffs.squaredNorm());
        }
        return gfa;
    }

    // Find local maxima of a function given as spherical harmonics coefficients by evaluation
    // on sampling directions
    template <typename data_t>
    std::tuple<DataContainer<data_t>, DataContainer<index_t>>
        extractFibers(const DataContainer<data_t>& sphericalCoeffs,
                      const DirVecList<data_t>& directions, const std::vector<Edge>& edges,
                      const index_t maximaCount, const bool clip)
    {

        const auto& etaDesc = downcast_safe<const SphericalCoefficientsDescriptor>(
            sphericalCoeffs.getDataDescriptor());
        const auto& volDesc = etaDesc.getDescriptorOfBlock(0);

        const auto dirCount = asSigned(directions.size());
        const auto volumeSize = volDesc.getNumberOfCoefficients();

        const VolumeDescriptor dirSpaceDesc({dirCount});
        const VolumeDescriptor coeffSpaceDesc({etaDesc.getNumberOfBlocks()});

        // Initialize magnitude with zeros such that missing maxima are irrelevant

        auto magnitude = zeros<data_t>(IdenticalBlocksDescriptor{maximaCount, volDesc});
        auto fiberIndices = empty<index_t>(IdenticalBlocksDescriptor{maximaCount, volDesc});

        SphericalFunctionTransform<data_t> evaluate{etaDesc, directions};

        auto possiblyClipped = [&]() {
            if (clip) {
                SphericalPositivity<data_t> clipping{etaDesc, directions};
                // Clip to positive, step length doesn't matter
                return clipping.proximal(sphericalCoeffs, 1.0);
            } else {
                return sphericalCoeffs;
            }
        }();

        FunkRadonTransform<data_t> funkRadon{etaDesc};

        auto orientationDensity = (evaluate * funkRadon).apply(possiblyClipped);

#pragma omp parallel for
        for (index_t volIdx = 0; volIdx < volumeSize; volIdx++) {

            auto isMax = localMaxima(orientationDensity, volIdx, edges);

            auto maxIndices = getSortedIndices(orientationDensity, volIdx, isMax);

            auto uniqueIndices = getUniqueIndices<data_t>(maxIndices, directions);

            // Store the localMax values in descending order up to the maximaCount
            // Note that r and i have shape (volume x maximaCount)
            index_t detectedMaxima = uniqueIndices.size();

            for (index_t fiber = 0; fiber < std::min(maximaCount, detectedMaxima); fiber++) {
                magnitude.getBlock(fiber)[volIdx] =
                    orientationDensity.getBlock(uniqueIndices[fiber])[volIdx];
                fiberIndices.getBlock(fiber)[volIdx] = uniqueIndices[fiber];
            }
        }

        return std::make_tuple(magnitude, fiberIndices);
    }

} // namespace detail

void add_visu_helpers(py::module& a)
{
    using namespace detail;
    a.def("extractFibersf", &extractFibers<float>, py::arg("sphericalCoeffs"),
          py::arg("directions"), py::arg("edges"), py::arg("maximaCount"), py::arg("clip"),
          py::return_value_policy::move);
    a.def("extractFibersd", &extractFibers<double>, py::arg("sphericalCoeffs"),
          py::arg("directions"), py::arg("edges"), py::arg("maximaCount"), py::arg("clip"),
          py::return_value_policy::move);

    a.attr("extractFibers") = a.attr("extractFibersf");
}

void add_weighting_functions(py::module& a)
{
    using namespace elsa::axdt;

    a.def("numericalWeightingFunctionf", &numericalWeightingFunction<float>,
          py::arg("rangeDescriptor"), py::arg("symmetry"), py::arg("degree"), py::arg("directions"),
          py::return_value_policy::move);
    a.def("numericalWeightingFunctiond", &numericalWeightingFunction<double>,
          py::arg("rangeDescriptor"), py::arg("symmetry"), py::arg("degree"), py::arg("directions"),
          py::return_value_policy::move);

    a.def("exactWeightingFunctionf", &exactWeightingFunction<float>, py::arg("rangeDescriptor"),
          py::arg("symmetry"), py::arg("degree"), py::return_value_policy::move);
    a.def("exactWeightingFunctiond", &exactWeightingFunction<double>, py::arg("rangeDescriptor"),
          py::arg("symmetry"), py::arg("degree"), py::return_value_policy::move);

    a.attr("numericalWeightingFunction") = a.attr("numericalWeightingFunctionf");
    a.attr("exactWeightingFunction") = a.attr("exactWeightingFunctionf");
}

void add_spherical_function_transform(py::module& a)
{
    using namespace elsa;
    using namespace elsa::axdt;

    py::class_<SphericalFunctionTransform<float>, LinearOperator<float>>(
        a, "SphericalFunctionTransformf")
        .def(py::init<const SphericalCoefficientsDescriptor&, const DirVecList<float>&>(),
             py::arg("domainDescriptor"), py::arg("directions"));

    py::class_<SphericalFunctionTransform<double>, LinearOperator<double>>(
        a, "SphericalFunctionTransformd")
        .def(py::init<const SphericalCoefficientsDescriptor&, const DirVecList<double>&>(),
             py::arg("domainDescriptor"), py::arg("directions"));

    a.attr("SphericalFunctionTransform") = a.attr("SphericalFunctionTransformf");
}

void add_funk_radon_transform(py::module& a)
{
    using namespace elsa;
    using namespace elsa::axdt;

    py::class_<FunkRadonTransform<float>, LinearOperator<float>>(a, "FunkRadonTransformf")
        .def(py::init<const SphericalCoefficientsDescriptor&>(), py::arg("descriptor"));

    py::class_<FunkRadonTransform<double>, LinearOperator<double>>(a, "FunkRadonTransformd")
        .def(py::init<const SphericalCoefficientsDescriptor&>(), py::arg("descriptor"));

    a.attr("FunkRadonTransform") = a.attr("FunkRadonTransformf");
}

void add_AXDT(py::module& m)
{
    using namespace elsa;
    using namespace elsa::axdt;

    auto axdt = m.def_submodule("axdt");

    py::enum_<Symmetry>(axdt, "Symmetry")
        .value("Even", Symmetry::even)
        .value("Regular", Symmetry::regular);

    add_axdt_operator(axdt);
    add_weighting_functions(axdt);
    add_spherical_function_transform(axdt);
    add_funk_radon_transform(axdt);
    add_visu_helpers(axdt);
}

void add_definitions_pyelsa_operators(py::module& m)
{
    add_finite_difference_difftype(m);
    add_identity(m);
    add_diagonal(m);
    add_scaling(m);
    add_finite_difference(m);
    add_symm_der(m);
    add_fourier_transform(m);
    add_block_op(m);
    add_shearlet_operator(m);
    add_AXDT(m);
    add_zero_operator(m);
    elsa::OperatorsHints::addCustomFunctions(m);
}

PYBIND11_MODULE(pyelsa_operators, m)
{
    add_definitions_pyelsa_operators(m);
}
