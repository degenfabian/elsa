#pragma once

#include <dlpack/dlpack.h>
/* dlpack mandates a pointer alignment of 256 */
#define DLPACK_ALIGNMENT (256)

#include <pybind11/pybind11.h>
#include <pybind11/complex.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>

#include "DataContainer.h"
#include "DataDescriptor.h"
#include "VolumeDescriptor.h"
#include "NdView.h"

#include <limits.h>

namespace detail
{

    namespace py = pybind11;

    template <class data_t>
    void tensor_destructor(DLManagedTensor* self)
    {
        delete[] self->dl_tensor.shape;
        /* strides is NULL, but might be needed in the future */
        delete[] self->dl_tensor.strides;
        elsa::mr::NativeContainer<data_t>* nc =
            static_cast<elsa::mr::NativeContainer<data_t>*>(self->manager_ctx);
        /* this is a bit redundant, deletion should do this as well*/
        nc->release();
        delete nc;
        delete self;
    }

    static void dlpack_capsule_destructor(PyObject* self)
    {
        /* used https://github.com/numpy/numpy/blob/main/numpy/core/src/multiarray/dlpack.c as
         * reference */

        /* exception free name check */
        if (PyCapsule_IsValid(self, "used_dltensor")) {
            return;
        }

        /* store exception, in case another is raised below*/
        PyObject *type, *value, *traceback;
        PyErr_Fetch(&type, &value, &traceback);

        DLManagedTensor* managed =
            reinterpret_cast<DLManagedTensor*>(PyCapsule_GetPointer(self, "dltensor"));
        if (managed == NULL) {
            PyErr_WriteUnraisable(self);
        } else if (managed->deleter) {
            managed->deleter(managed);
        }

        PyErr_Restore(type, value, traceback);
    }

    template <class data_t>
    py::object build_data_container(DLManagedTensor* tensor, py::capsule& capsule,
                                    elsa::DataDescriptor* descriptor_opt)
    {
        static_assert(std::is_trivially_copyable<data_t>::value);

        DLDevice device = tensor->dl_tensor.device;
        elsa::index_t ndim = tensor->dl_tensor.ndim;
        elsa::IndexVector_t shape(ndim);
        elsa::IndexVector_t strides(ndim);
        for (elsa::index_t i = 0; i < ndim; i++) {
            shape(i) = tensor->dl_tensor.shape[i];
        }
        if (tensor->dl_tensor.strides) {
            for (elsa::index_t i = 0; i < ndim; i++) {
                strides(i) = tensor->dl_tensor.strides[i];
            }
        } else {
            if (ndim > 0) {
                strides(ndim - 1) = 1;
                for (elsa::index_t i = ndim - 1; i > 0; --i) {
                    strides(i - 1) = tensor->dl_tensor.shape[i] * strides(i);
                }
            }
        }

        /* elsa's indexing convention is opposite that of tensor libraries ([column, row, depth,
         * ...])*/
        shape.reverseInPlace();
        strides.reverseInPlace();

        capsule.set_name("used_dltensor");

        data_t* imported_data_begin = reinterpret_cast<data_t*>(
            reinterpret_cast<uintptr_t>(tensor->dl_tensor.data) + tensor->dl_tensor.byte_offset);

        elsa::mr::StorageType storage_type;
        if (tensor->dl_tensor.device.device_type == kDLCPU) {
            storage_type = elsa::mr::StorageType::host;
        } else if (tensor->dl_tensor.device.device_type == kDLCUDA) {
            storage_type = elsa::mr::StorageType::device;
        } else if (tensor->dl_tensor.device.device_type == kDLCUDAManaged
                   || tensor->dl_tensor.device.device_type == kDLCUDAHost) {
            storage_type = elsa::mr::StorageType::universal;
        } else {
            return py::none();
        }

        if (descriptor_opt) {
            return py::cast(std::make_unique<elsa::DataContainer<data_t>>(
                elsa::DataContainer<data_t>::fromRawData(imported_data_begin, storage_type, shape,
                                                         strides, *descriptor_opt,
                                                         [tensor]() { tensor->deleter(tensor); })));
        } else {
            return py::cast(std::make_unique<elsa::DataContainer<data_t>>(
                elsa::DataContainer<data_t>::fromRawData(imported_data_begin, storage_type, shape,
                                                         strides, elsa::VolumeDescriptor(shape),
                                                         [tensor]() { tensor->deleter(tensor); })));
        }
    }

    DLDevice dlpack_device()
    {
        DLDeviceType dl_device_type;
        switch (elsa::mr::sysStorageType) {
            case elsa::mr::StorageType::host:
                dl_device_type = kDLCPU;
                break;
            case elsa::mr::StorageType::universal:
                dl_device_type = kDLCUDAManaged;
                break;
            default:
                throw std::runtime_error("Internal error! Unsupported storage type.");
        }

        DLDevice dl_device;
        dl_device.device_type = dl_device_type;
        /* 0 is expected for both managed CUDA memory and regular host memory */
        dl_device.device_id = 0;
        return dl_device;
    }

    template <class data_t>
    py::object data_container_dlpack(elsa::DataContainer<data_t>& self, py::object stream)
    {
        /* for now, support only stream none, like numpy */
        if (!stream.is(py::none())) {
            PyErr_SetString(PyExc_RuntimeError, "elsa only supports stream=None.");
            return py::none();
        }

        DLDataType dl_type;
        dl_type.bits = CHAR_BIT * sizeof(data_t);
        dl_type.lanes = 1;

        if constexpr (std::is_same<data_t, bool>::value) {
            dl_type.code = kDLBool;
        } else if constexpr (std::is_floating_point<data_t>::value) {
            dl_type.code = kDLFloat;
        } else if constexpr (std::is_integral<data_t>::value) {
            if constexpr (std::is_unsigned<data_t>::value) {
                dl_type.code = kDLUInt;
            } else {
                dl_type.code = kDLInt;
            }
        } else if (std::is_same<data_t, elsa::complex<float>>::value
                   || std::is_same<data_t, elsa::complex<double>>::value) {
            dl_type.code = kDLComplex;
        } else {
            PyErr_SetString(PyExc_RuntimeError,
                            "__dlpack__ unsupported for data container of this type.");
            return py::none();
        }

        DLDevice dl_device = dlpack_device();

        auto& desc = self.getDataDescriptor();
        int32_t ndim = desc.getNumberOfDimensions();
        /* use unique pointers for exception safety */
        std::unique_ptr<int64_t[]> shape = std::make_unique<int64_t[]>(ndim);
        std::unique_ptr<int64_t[]> strides = std::make_unique<int64_t[]>(ndim);
        /* Note that shape and strides are reversed, because elsa's indexing convention is
         * opposite that of tensor libraries ([column, row, depth, ...]) */
        elsa::IndexVector_t shape_vector = desc.getNumberOfCoefficientsPerDimension().reverse();
        elsa::IndexVector_t strides_vector = desc.getProductOfCoefficientsPerDimension().reverse();
        for (size_t i = 0; i < ndim; i++) {
            shape[i] = shape_vector(i);
            strides[i] = strides_vector(i);
        }

        std::unique_ptr<DLManagedTensor> managed = std::make_unique<DLManagedTensor>();

        managed->dl_tensor.device = dl_device;
        managed->dl_tensor.dtype = dl_type;
        managed->dl_tensor.ndim = ndim;
        managed->dl_tensor.shape = shape.get();
        /* according to the spec, NULL signifies row-major, densely packed layout */
        managed->dl_tensor.strides = strides.get();

        std::unique_ptr<elsa::mr::NativeContainer<data_t>> raw_data =
            std::make_unique<elsa::mr::NativeContainer<data_t>>(self.storage().lock_native());
        managed->manager_ctx = raw_data.get();

        uintptr_t data_ptr = reinterpret_cast<uintptr_t>(raw_data->raw_pointer);
        uintptr_t aligned_data_ptr = data_ptr & ~(DLPACK_ALIGNMENT - 1);
        managed->dl_tensor.data = reinterpret_cast<void*>(aligned_data_ptr);
        managed->dl_tensor.byte_offset = data_ptr - aligned_data_ptr;

        managed->deleter = tensor_destructor<data_t>;

        /* release unique pointers so that lifetime is controlled by
           capsule destructor from now on */
        shape.release();
        strides.release();
        raw_data.release();
        return py::capsule(managed.release(), "dltensor", dlpack_capsule_destructor);
    }

    template <class data_t>
    py::object data_container_from_dlpack(py::object obj, py::object descriptor)
    {
        py::capsule capsule = obj.attr("__dlpack__")();
        if (capsule.is(py::none())) {
            PyErr_SetString(PyExc_RuntimeError, "object does not implement __dlpack__!");
            return py::none();
        }

        if (std::strcmp(capsule.name(), "dltensor") != 0) {
            PyErr_SetString(PyExc_RuntimeError, "result of __dlpack__ does not contain a tensor.");
            return py::none();
        }

        /* get_pointer never returns null (defined in pytypes.h, nothing in pybind is properly
         * documented) */
        DLManagedTensor* tensor = capsule.get_pointer<DLManagedTensor>();

        DLDataType dtype = tensor->dl_tensor.dtype;
        if (dtype.lanes != 1) {
            PyErr_SetString(PyExc_RuntimeError, "unsupported data type.");
            return py::none();
        }

        elsa::DataDescriptor* desc_opt = nullptr;
        if (!descriptor.is(py::none())) {
            desc_opt = descriptor.cast<elsa::DataDescriptor*>();
        }

        switch (dtype.code) {
            case kDLFloat:
                if (dtype.bits == sizeof(float) * CHAR_BIT) {
                    return build_data_container<float>(tensor, capsule, desc_opt);
                } else if (dtype.bits == sizeof(double) * CHAR_BIT) {
                    return build_data_container<double>(tensor, capsule, desc_opt);
                }
                break;
            case kDLComplex:
                if (dtype.bits == sizeof(elsa::complex<float>) * CHAR_BIT) {
                    return build_data_container<elsa::complex<float>>(tensor, capsule, desc_opt);
                } else if (dtype.bits == sizeof(elsa::complex<double>) * CHAR_BIT) {
                    return build_data_container<elsa::complex<double>>(tensor, capsule, desc_opt);
                }
                break;
            case kDLInt:
                if (dtype.bits == sizeof(long) * CHAR_BIT) {
                    return build_data_container<long>(tensor, capsule, desc_opt);
                }
                break;
        }

        PyErr_SetString(PyExc_RuntimeError, "unsupported data type.");
        return py::none();
    }

    template <class data_t>
    void add_data_container_dlpack(py::module& m, py::class_<elsa::DataContainer<data_t>> dc)
    {
        dc.def("__dlpack__", data_container_dlpack<data_t>, py::arg("stream") = py::none());
        dc.def("__dlpack_device__", [](elsa::DataContainer<data_t>& self) {
            DLDevice dl_device = dlpack_device();
            return py::make_tuple(py::cast(static_cast<int32_t>(dl_device.device_type)),
                                  py::cast(dl_device.device_id));
        });

        m.def("from_dlpack", data_container_from_dlpack<data_t>, py::arg("x"),
              py::arg("descriptor") = py::none());
    }
} // namespace detail
